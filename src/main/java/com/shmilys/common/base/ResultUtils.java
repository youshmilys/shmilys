package com.shmilys.common.base;
/**
 * 功能之一 可以用于增删改返回状态。
 *  功能之二 用于前端提示 比如：
 *   @ApiOperation(value = "导入公司信息")
 *     @PostMapping(value = "/import")
 *     public BaseResult importCompany(@RequestParam("file") MultipartFile file, String id, int type) throws IOException {
 *
 *         BaseResult result = BaseResult.succResult();
 *
 *         if (file.isEmpty()) {
 *             return ResultUtils.failResult("请选择上传文件");
 *         }
 *         String filename = file.getOriginalFilename();
 *         if (!filename.endsWith(".xlsx")) {
 *             return ResultUtils.failResult("文件格式不对，请下载模版");
 *         }
 *         Workbook wb = new XSSFWorkbook(file.getInputStream());
 *         try {
 *             return collectCompanyBusiness.importCompany(wb, id, type, ContextUtil.getContext());
 * //            collectFundBusiness.sync(ContextUtil.getContext(), arg.getId());
 *         } catch (BusinessException e) {
 *             ErrorHandle.bussinessExceptionHandle(result, "importFund", e);
 *             return ResultUtils.failResult(-1, e.getMessage());
 *         } catch (Exception e) {
 *             ErrorHandle.systemExceptionHandle(result, "importFund", e);
 *             return ResultUtils.failResult(-1, e.getMessage());
 *         }
 *     }
* @Description: 结果工具类
**/
public class ResultUtils {

    public static BaseResult successResult(Object data) {
        BaseResult result = BaseResult.succResult();
        result.setResult(data);
        return result;
    }

    public static BaseResult successResult(String message,Object data){
        BaseResult result = BaseResult.httpOkResult(message);
        result.setResult(data);
        return result;
    }

    public static BaseResult successResult(int code,String message,Object data){
        BaseResult result =  BaseResult.httpOkResult(message);
        result.setCode(code);
        result.setResult(data);
        return result;
    }

    /**
     * 创建错误消息
     * @param errorMsg
     * @return com.jingdata.common.response.BaseResult
     */
    public static BaseResult failResult(String errorMsg) {
        BaseResult result = BaseResult.error(errorMsg);
        result.setMessage(errorMsg);
        return result;
    }

    public static BaseResult failResult(int code, String errorMsg) {
        BaseResult result = BaseResult.error(errorMsg);
        result.setMessage(errorMsg);
        result.setCode(code);
        return result;
    }

    public static <T> BaseResult failResult(int code, String errorMsg, T data) {
        BaseResult<T> result = BaseResult.error(errorMsg);
        result.setMessage(errorMsg);
        result.setCode(code);
        result.setResult(data);
        return result;
    }

}
