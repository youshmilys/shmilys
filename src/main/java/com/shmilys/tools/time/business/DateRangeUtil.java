package com.shmilys.tools.time.business;

import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateRangeUtil {

    /**
     * 获取某年第几周的开始日期
     *
     * @param year      某年
     * @param weekIndex 第几周
     * @return 时间戳格式的日期
     */
    public static Timestamp getStartDateOfWeek(int year, int weekIndex) {
        Calendar cal = getCalendarForWeek(year);
        cal.set(Calendar.WEEK_OF_YEAR, weekIndex);
        return Timestamp.valueOf(getCalendarToTimestampStr(setStartHhMmSs(cal)));
    }

    /**
     * 获取某年第几周的结束日期
     *
     * @param year      某年
     * @param weekIndex 第几周
     * @return 时间戳格式的日期
     */
    public static Timestamp getEndDateOfWeek(int year, int weekIndex) {
        Calendar cal = getCalendarForWeek(year);
        cal.set(Calendar.WEEK_OF_YEAR, weekIndex);
        cal.add(Calendar.DAY_OF_WEEK, 6);
        return Timestamp.valueOf(getCalendarToTimestampStr(setEndHhMmSs(cal)));
    }


    /**
     * 获取某年某月的开始日期
     *
     * @param year       某年
     * @param monthIndex 某月
     * @return 时间戳格式的日期
     */
    public static Timestamp getStartDateOfMonth(int year, int monthIndex) {
        Calendar cal = getCalendarForMonth(year, monthIndex);
        return Timestamp.valueOf(getCalendarToTimestampStr(setStartHhMmSs(cal)));
    }

    /**
     * 获取某年某月的结束日期
     *
     * @param year       某年
     * @param monthIndex 某月
     * @return 时间戳格式的日期
     */
    public static Timestamp getEndDateOfMonth(int year, int monthIndex) {
        Calendar cal = getCalendarForMonth(year, monthIndex);
        int total = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.add(Calendar.DAY_OF_MONTH, total - 1);
        return Timestamp.valueOf(getCalendarToTimestampStr(setEndHhMmSs(cal)));
    }

    /**
     * 获取某年第几季度的开始日期
     *
     * @param year         某年
     * @param quarterIndex 第几季度
     * @return 时间戳格式的日期
     */
    public static Timestamp getStartDateOfQuarter(int year, int quarterIndex) {
        Calendar cal = getCalendarForQuarter(year, quarterIndex);
        return Timestamp.valueOf(getCalendarToTimestampStr(setStartHhMmSs(cal)));
    }

    /**
     * 获取某年第几季度的结束日期
     *
     * @param year         某年
     * @param quarterIndex 第几季度
     * @return 时间戳格式的日期
     */
    public static Timestamp getEndDateOfQuarter(int year, int quarterIndex) {
        Calendar cal = getCalendarForQuarter(year, quarterIndex);
        cal.add(Calendar.MONTH, 2);
        int total = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.add(Calendar.DAY_OF_MONTH, total - 1);
        return Timestamp.valueOf(getCalendarToTimestampStr(setEndHhMmSs(cal)));
    }


    /**
     * 获取某年的开始日期
     *
     * @param year 某年
     * @return 时间戳格式的日期
     */
    public static Timestamp getStartDateOfYear(int year) {
        Calendar cal = getCalendarForYear(year);
        return Timestamp.valueOf(getCalendarToTimestampStr(setStartHhMmSs(cal)));
    }

    /**
     * 获取某年的结束日期
     *
     * @param year 某年
     * @return 时间戳格式的日期
     */
    public static Timestamp getEndDateOfYear(int year) {
        Calendar cal = getCalendarForYear(year);
        cal.add(Calendar.MONTH, 11);
        int total = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.add(Calendar.DAY_OF_MONTH, total - 1);
        return Timestamp.valueOf(getCalendarToTimestampStr(setEndHhMmSs(cal)));
    }


    private static Calendar getCalendarForWeek(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.YEAR, year);
        return cal;
    }

    private static Calendar getCalendarForMonth(int year, int monthIndex) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthIndex - 1);
        cal.set(Calendar.DATE, 1);
        return cal;
    }

    private static Calendar getCalendarForQuarter(int year, int quarterIndex) {
        int startMonth = (quarterIndex - 1) * 3 + 1;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, startMonth - 1);
        cal.set(Calendar.DATE, 1);
        return cal;
    }

    /**
     * 获取 ’指定年‘ 的 ‘当前开始‘ 时间
     *
     * @param year
     * @return
     */
    private static Calendar getCalendarForYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DATE, 1);
        return cal;
    }


    /**
     * 设置 开始时间的 时分秒
     *
     * @param cal
     * @return
     */
    private static Calendar setStartHhMmSs(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal;
    }

    /**
     * 设置 开始时间的 时分秒
     *
     * @param cal
     * @return
     */
    private static Calendar setEndHhMmSs(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal;
    }

    /**
     * 获取 日历 的 时间戳字符串 （月 需要 +1）
     *
     * @param cal
     * @return
     */
    private static String getCalendarToTimestampStr(Calendar cal) {
        return new StringBuilder().append(cal.get(Calendar.YEAR))                                //年
                        .append("-").append(cal.get(Calendar.MONTH) + 1)         //月
                        .append("-").append(cal.get(Calendar.DAY_OF_MONTH))      //日
                        .append(" ")
                        .append(cal.get(Calendar.HOUR_OF_DAY))                   //时
                        .append(":").append(cal.get(Calendar.MINUTE))            //分
                        .append(":").append(cal.get(Calendar.SECOND))            //秒
                        .append(".").append(cal.get(Calendar.MILLISECOND))     //毫秒
                        .toString();


//        String yyyyMmDd = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH);
//        String hhMmSs = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
//        return yyyyMmDd + " "+ hhMmSs;
    }


    /**
     * 获取date的月份的时间范围
     *
     * @param date
     * @return
     */
    public static DateRange getMonthRange(Date date) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        setMaxTime(startCalendar);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(date);
        endCalendar.set(Calendar.DAY_OF_MONTH, endCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(endCalendar);

        return new DateRange(startCalendar.getTime(), endCalendar.getTime());
    }

    /**
     * 获取指定日期的当前季度的时间范围
     *
     * @return current quarter
     */
    public static DateRange getThisQuarter(Date date) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        startCalendar.set(Calendar.MONTH, (startCalendar.get(Calendar.MONTH) / 3) * 3);
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        setMinTime(startCalendar);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(date);
        endCalendar.set(Calendar.MONTH, (startCalendar.get(Calendar.MONTH) / 3) * 3 + 2);
        endCalendar.set(Calendar.DAY_OF_MONTH, endCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(endCalendar);
        return new DateRange(startCalendar.getTime(), endCalendar.getTime());
    }

    /**
     * 获取指定日期的上个季度的时间范围
     *
     * @return
     */
    public static DateRange getLastQuarter(Date date) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        startCalendar.set(Calendar.MONTH, (startCalendar.get(Calendar.MONTH) / 3 - 1) * 3);
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        setMinTime(startCalendar);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(date);
        endCalendar.set(Calendar.MONTH, (endCalendar.get(Calendar.MONTH) / 3 - 1) * 3 + 2);
        endCalendar.set(Calendar.DAY_OF_MONTH, endCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(endCalendar);

        return new DateRange(startCalendar.getTime(), endCalendar.getTime());
    }
    /**
     * 获取昨天的时间范围
     *
     * @return
     */
    public static DateRange getYesterdayRange() {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.add(Calendar.DAY_OF_MONTH, -1);
        setMinTime(startCalendar);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.add(Calendar.DAY_OF_MONTH, -1);
        setMaxTime(endCalendar);

        return new DateRange(startCalendar.getTime(), endCalendar.getTime());
    }

    /**
     * 获取当前月份的时间范围
     *
     * @return
     */
    public static DateRange getThisMonth() {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        setMinTime(startCalendar);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.DAY_OF_MONTH, endCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(endCalendar);

        return new DateRange(startCalendar.getTime(), endCalendar.getTime());
    }

    /**
     * 获取上个月的时间范围
     *
     * @return
     */
    public static DateRange getLastMonth() {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.add(Calendar.MONTH, -1);
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        setMinTime(startCalendar);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.add(Calendar.MONTH, -1);
        endCalendar.set(Calendar.DAY_OF_MONTH, endCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setMaxTime(endCalendar);

        return new DateRange(startCalendar.getTime(), endCalendar.getTime());
    }



    private static void setMinTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private static void setMaxTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));
    }

    public static String format(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time;
        try {
            time =  sdf.format(date);
        }catch (Exception e){
            return null;
        }
        return time;
    }

    /**
     *  时间是否在start 和 end 之间
     * @param start
     * @param end
     * @param dt1
     * @return
     */
    public static boolean dateRange(DateTime start, DateTime end , DateTime dt1) {

        int i = end.toLocalDate().compareTo(start.toLocalDate());
        if(i < 0){
            // 传入的时间 end < start
            return false;
        }

        int c1 = dt1.toLocalDate().compareTo(start.toLocalDate());
        // 0 1

        int c2 = dt1.toLocalDate().compareTo(end.toLocalDate());
        // -1 0

        if(c1 >=0 && c2 <= 0){
            return true;
        }

        return false;
    }

//    public static void main(String[] args) {
//
//        DateRange currentQuarter = getThisQuarter();
//        DateRange yesterdayRange = getYesterdayRange();
//        System.out.println("昨天的时间范围: "+DateRangeUtil.format(yesterdayRange.getStart())+" - "+DateRangeUtil.format(yesterdayRange.getEnd()));
//
//        DateRange thisMonth = getThisMonth();
//        System.out.println("当前月份的时间范围: "+DateRangeUtil.format(thisMonth.getStart())+" - "+DateRangeUtil.format(thisMonth.getEnd()));
//
//        DateRange lastMonth = getLastMonth();
//        System.out.println("上个月的时间范围: "+DateRangeUtil.format(lastMonth.getStart())+" - "+DateRangeUtil.format(lastMonth.getEnd()));
//
//
//        System.out.println("当前季度的时间范围： "+ DateRangeUtil.format(currentQuarter.getStart())+" - "+DateRangeUtil.format(currentQuarter.getEnd()));
//        DateRange lastQuarter = getLastQuarter();
//        System.out.println("上个季度的时间范围: "+DateRangeUtil.format(lastQuarter.getStart())+" - "+DateRangeUtil.format(lastQuarter.getEnd()));
//
//    }

}


