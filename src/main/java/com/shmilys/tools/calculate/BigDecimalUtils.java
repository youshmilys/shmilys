package com.shmilys.tools.calculate;

import com.google.gson.JsonObject;
import com.shmilys.common.exception.BusinessException;
import com.shmilys.tools.format.MessageFormatUtils;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.regex.Pattern;

@Slf4j
public class BigDecimalUtils {

    public static final Pattern PATTERN = Pattern.compile("\\d+(.\\d+)?$");

    public static final int DEF_SCALE = 2;

    /* --------------------------> 检查参数相关 <------------------------------- */


//    public static Object compareValue(BigDecimal big1, BigDecimal big2, Object defaultValue){
//        if (!verify(big1) && !verify(big2)){
//            return defaultValue;
//        }
//        if(big1.compareTo(big2) == 0){
//            return "0.00";
//        }
//
//    }

    public static Object verifyBig(BigDecimal big){
        if (big == null || BigDecimal.ZERO.compareTo(big) == 0) {
            return null;
        }
        return big;
    }


    /**
     *  检查参数的值
     * @param big
     * @return true -> 有值 ,  false -> 无值
     */
    public static boolean verify(BigDecimal big){
        return big != null;
    }

    /**
     * 检查检查参数的值是否为空或等于0
     * @param big
     * @return
     */
    public static BigDecimal verifyValue(BigDecimal big){
        if (!verify(big) && BigDecimal.ZERO.compareTo(big) == 0){
            return null;
        }
        return big;
    }

    public static BigDecimal verifyResult(BigDecimal big){
        if (!verify(big)){
            return null;
        }
        return big;
    }

    /**
     *  检查参数的值：
     *      不存在：默认返回 0
     *      存在：返回本身值
     * @param big
     * @return
     */
    public static BigDecimal verifyResultDefaultZero(BigDecimal big){
        return verifyResult(big, BigDecimal.ZERO);
    }

    /**
     *  检查参数的值：
     *      不存在：默认返回 null
     *      存在：返回本身值
     * @param big
     * @return
     */
    public static BigDecimal verifyResultDefaultNone(BigDecimal big){
        return verifyResult(big, null);
    }

    /**
     *  检查参数的值：
     *      不存在：默认返回空，如果有传入默认值：会返回默认值。
     *      存在：返回本身值
     * @param big
     * @return
     */
    public static BigDecimal verifyResult(BigDecimal big, BigDecimal defaultValue) {
        if (!verify(big)){
            if (defaultValue == null) {
                return BigDecimal.ZERO;
            } else {
                return defaultValue;
            }
        }
        return big;
    }
    /**
     *  默认保留两位小数
     *  默认舍入方式 ‘四舍五入’
     * @param big 校验处理的值
     * @return
     */
    public static BigDecimal verifyHandleResult(BigDecimal big){

        return verifyRoundingModeResult(big,2);
    }

    /**
     *  自定义‘保留小数位’
     *  舍入方式默认‘四舍五入’
     *
     *  如果自定义保留小数为空 则：
     *      默认‘保留两位小数位’
     *      默认舍入方式 ‘四舍五入’
     * @param big 校验处理的值
     * @param newScale 保留几位小数位
     * @return
     */
    public static BigDecimal verifyRoundingModeResult(BigDecimal big,Integer newScale){
        return verifyRangeAndRoundingModeResult(big,newScale,BigDecimal.ROUND_HALF_UP);
    }

    /**
     *  自定义‘保留小数位’
     *  自定义‘舍入方式’
     *
     *  如果舍入方式为空 则：
     *      自定义‘保留小数位’
     *      舍入方式默认‘四舍五入’
     * @param big           校验处理的值
     * @param newScale      保留几位小数位
     * @param roundingMode  舍入方式
     * @return
     */
    public static BigDecimal verifyRangeAndRoundingModeResult(BigDecimal big, int newScale, Integer roundingMode){
        if (roundingMode == null){
            return verifyRoundingModeResult(big,newScale);
        }
        return verifyHandle(big, newScale, roundingMode);
    }

    /**
     * @param big           校验处理的值
     * @param newScale      保留几位小数位
     * @param roundingMode  舍入方式
     * @return
     */
    private static BigDecimal verifyHandle(BigDecimal big, int newScale, int roundingMode){

        if (!verify(big)) { return null; }

        if (newScale >= BigDecimal.ROUND_UP){
            //为了防止报无效舍入模式的异常。
            if (roundingMode < BigDecimal.ROUND_UP || roundingMode > BigDecimal.ROUND_UNNECESSARY){
                //'自定义保留小数' 并 '自定义舍入模式'
                return big.setScale(newScale, roundingMode);
            }
            //如果'自定义舍入模式'为空 默认 ‘自定义保留小数位’ 并 ‘四舍五入’
            return big.setScale(newScale, BigDecimal.ROUND_HALF_UP);
        }
        //如果'自定义保留小数位'为空 默认 ‘保留两位’ 并 ‘四舍五入’
        return big.setScale(BigDecimal.ROUND_CEILING, BigDecimal.ROUND_HALF_UP);
    }


    /**
     * 是否数字
     */
    public static boolean verifyIsNumber(Object object) {
        return PATTERN.matcher(object.toString()).matches();
    }


    /* ----------------------------> big加减乘除运算 <----------------------------- */

    /**
     * 提供数据类型转换为BigDecimal
     *
     * @param object 原始数据
     * @return BigDecimal
     */
    public static BigDecimal bigDecimal(Object object) {
        BigDecimal result;
        try {
            String isNumber = String.valueOf(object).replaceAll(",", "");
            result = new BigDecimal(isNumber);
        } catch (NumberFormatException e) {
            log.error(MessageFormat.format("Please give me a numeral.Not Param Data ：{0}", object));
            return BigDecimal.ZERO;
//            throw new NumberFormatException("Please give me a numeral.Not Param Data ：" + object);
        }
        return verifyResultDefaultNone(result);
    }

    /**
     * 提供精确的加法运算。
     *
     * @param big1 被加数
     * @param big2 加数
     * @return 两个参数的和
     */
    public static BigDecimal add(Object big1, Object big2) {
        return bigDecimal(big1).add(bigDecimal(big2));
    }

    /**
     * 提供(相对)精确的减法运算。
     *
     * @param big1 被减数
     * @param big2 减数
     * @return 两个参数的差
     */
    public static BigDecimal subtract(Object big1, Object big2) {
        return bigDecimal(big1).subtract(bigDecimal(big2));
    }

    /**
     * 提供(相对)精确的乘法运算。
     *
     * @param big1 被乘数
     * @param big2 乘数
     * @return 两个参数的积
     */
    public static BigDecimal multiply(Object big1, Object big2) {
        return bigDecimal(big1).multiply(bigDecimal(big2));
    }

    /**
     * 提供(相对)精确的除法运算，当发生除不尽的情况时，精度为10位，以后的数字四舍五入。
     *
     * @param big1 被除数
     * @param big2 除数
     * @return 两个参数的商
     */
    public static BigDecimal divide(Object big1, Object big2) {
        return divide(big1, big2, DEF_SCALE);
    }

    /**
     * 提供(相对)精确的除法运算。 当发生除不尽的情况时，由scale参数指定精度，以后的数字四舍五入。
     *
     * @param num1  被除数
     * @param num2  除数
     * @param scale 表示表示需要精确到小数点以后几位。
     * @return 两个参数的商
     */
    public static BigDecimal divide(Object num1, Object num2, Integer scale) {
        if (scale == null) {
            scale = DEF_SCALE;
        }
        num2 = num2 == null || Math.abs(new Double(num2.toString())) == 0 ? 1 : num2;
        return bigDecimal(num1).divide(bigDecimal(num2), scale, BigDecimal.ROUND_HALF_UP);
    }

    /* -----------------> jsonObject <---------------- */

    public static BigDecimal getJsonBigDecimal(JsonObject jsonObject, String key) {
        if (jsonObject.get(key).isJsonNull() || "".equals(jsonObject.get(key).getAsString())) {
            return BigDecimal.ZERO;
        }
        return jsonObject.get(key).getAsBigDecimal();
    }


    /* ----------------------------- big计算后 特殊处理  ------------------------------------ */

    /**
     * 是否是负数
     * @param big
     * @return
     */
    public static boolean isNegative(BigDecimal big){
        if (big == null){
            log.error("method isNegative big 参数为空");
            throw new BusinessException(-1, MessageFormatUtils.msgFormat("method isNegative big 参数为空"));
        }
        return big.compareTo(BigDecimal.ZERO) <= 0;
    }

    /**
     *  判断差值为零时 返回0.00
     * @param big1
     * @param big2
     * @return
     */
    public static boolean isDifferenceZero(BigDecimal big1, BigDecimal big2){
        if (big1 == null || big2 == null){
            throw new BusinessException(-2, MessageFormatUtils.msgFormat("method differenceZeroDefaultZero 参数值为空!"));
        }
        return big1.compareTo(big2) == 0;
    }

    /**
     * 如果big是负数则返回0.00
     * 反之返回自己
     * @param big
     * @return
     */
    public static BigDecimal negativeDefaultZero(BigDecimal big){
        if (isNegative(big)){
            return BigDecimal.ZERO;
        }
        return big;
    }

    /**
     *  如果两数都为空 则返回空
     *  如果两数都为0  则返回空
     *  如果两数差值为0则返回默认0
     *  反之返回 big1 - big2 = 结果
     * @param big1
     * @param big2
     * @return
     */
    public static BigDecimal differenceZeroDefaultZero(BigDecimal big1 , BigDecimal big2){
        return differenceZeroCustomData(big1, big2, BigDecimal.ZERO);
    }


    /**
     *  如果两数都为空 则返回空
     *  如果两数都为0  则返回空
     *  如果两数都有值 并且差值为 0 则返回指定默认值
     *  反之返回 big1 - big2 = 结果
     * @param big1
     * @param big2
     * @return
     */
    public static BigDecimal differenceZeroCustomData(BigDecimal big1 , BigDecimal big2,BigDecimal defaultBig){
        if (big1 == null && big2 == null ){ return null; }
        if (BigDecimal.ZERO.compareTo(big1) == 0 && BigDecimal.ZERO.compareTo(big2) == 0){ return null; }
        if (isDifferenceZero(big1, big2)){ return defaultBig; }
        return subtract(big1,big2);
    }

    public static void main(String[] args) {
        System.out.println(differenceZeroDefaultZero(new BigDecimal("10"),new BigDecimal("10")));
    }


}
