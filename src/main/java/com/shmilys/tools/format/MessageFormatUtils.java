package com.shmilys.tools.format;

import java.text.MessageFormat;

public class MessageFormatUtils {

    private static final String MSG = "";

    /**
     * 在原有的消息中 额外增加提醒
     */
    public static String msgFormat(String msg, Object... infoData) {

        if (infoData.length == 0){ return MessageFormat.format(msg, infoData); }

        return magFormatIncrease(msg, infoData);
    }

    /**
     * 格式化消息
     */
    public static String format(Object... infoData){
        return magFormatIncrease(MSG, infoData);
    }

    private static String magFormatIncrease(String msg, Object[] infoData) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < infoData.length; i++) {
            sign.append("{").append(i).append("}");
        }
        return MessageFormat.format(msg.concat(sign.toString()), infoData);
    }
}
