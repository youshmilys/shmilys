package com.shmilys.tools.collection;

import java.util.Collection;

public class VerifyCollectionUtils {

    public static boolean isNotEmpty(Collection<?> coll) {
        return !isEmpty(coll);
    }

    public static boolean isEmpty(Collection<?> coll) {
        return coll == null || coll.isEmpty();
    }


}
