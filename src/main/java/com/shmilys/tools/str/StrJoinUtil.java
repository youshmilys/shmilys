package com.shmilys.tools.str;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class StrJoinUtil {

    public static String paresStrToStrListHaveDef(List<String> sourceList,final String sign,String defaultStr){
        String str = paresStrToStrList(sourceList, sign);
        if (StringUtils.isBlank(str)){ return defaultStr;}
        return str;

    }

    /**
     * StrJoin 防止空指针
     * @param sourceList /
     * @param sign /
     * @return /
     */
    public static String paresStrToStrList(List<String> sourceList,final String sign){
        if (CollectionUtils.isEmpty(sourceList)){ return null; }
        return String.join(sign,sourceList);
    }


//    public static void main(String[] args) {
//        List<Integer> list = new ArrayList<>();
//        IntStream.range(0, 10000000).boxed()
//                .forEach(i -> list.add(i));
//
//        int MAX_HANDLE = 100000;
//        int mAXHANDLETOTAL = (list.size() + MAX_HANDLE - 1) / MAX_HANDLE;
//
//
//        //数据分割 skip 手动塞数据
//        long start = System.currentTimeMillis();
//        List<List<Integer>> result1 = new ArrayList<>(mAXHANDLETOTAL);
//        Stream.iterate(0, n -> n + 1)
//                .limit(mAXHANDLETOTAL)
//                .parallel()
//                .forEach(i -> {
//                    int fromIndex = i * MAX_HANDLE;
//                    int toIndex = fromIndex + MAX_HANDLE;
//                    result1.add(list.stream().skip(fromIndex).limit(toIndex).collect(Collectors.toList()));
//                });
//
//        System.err.println("Stream skip  手动塞数据 总耗时：" + (System.currentTimeMillis() - start) + "ms");
//
//
//        //数据分割 skip collect收集数据
//        start = System.currentTimeMillis();
//        List<List<Integer>> result2 = IntStream.range(0, mAXHANDLETOTAL)
//                .boxed()
//                .parallel()
//                .map(i -> {
//                    int fromIndex = i * MAX_HANDLE;
//                    int toIndex = fromIndex + MAX_HANDLE;
//                    return list.stream().skip(fromIndex).limit(toIndex).collect(Collectors.toList());
//                })
//                .collect(Collectors.toList());
//    }

}
