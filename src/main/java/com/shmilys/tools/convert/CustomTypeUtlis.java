package com.shmilys.tools.convert;

import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class CustomTypeUtlis {

    /*  ----------------------     自定义类型转换类型  ------------------------ */

    public static <T> T customConvertType(Object value, Class<T> clazz) {
        if (Objects.isNull(value)) {
            return null;
        } else if (clazz.isInstance(value)) {
            return (T) value;
        } else if ((value instanceof Long) && Objects.equals(clazz, Date.class)) {
            return (T) new Date(Long.parseLong(value.toString()));
        } else if (value instanceof Integer && Objects.equals(clazz, Long.class)) {
            return (T) Long.valueOf(((Integer) value).longValue());
        } else if (Objects.equals(clazz, Integer.class)) {
            return (T) Integer.valueOf(String.valueOf(value));
        } else if (Objects.equals(clazz, Boolean.class)) {
            return (T) Boolean.valueOf(String.valueOf(value));
        } else if (Objects.equals(clazz, BigDecimal.class)) {
            return (T) new BigDecimal(String.valueOf(value));
        } else if (Objects.equals(clazz, List.class)) {
            String str;
            if (value instanceof String) {
                str = (String) value;
            } else {
                str = JSON.toJSONString(value);
            }
            return (T) JSON.parseArray(str, String.class);
        } else if (Objects.equals(clazz, String.class)) {
            return (T) String.valueOf(value);
        } else {
            return clazz.cast(value);
        }
    }
}
