package com.shmilys.common.exception;

import com.shmilys.common.base.BaseResult;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shmilys
 * @date 2020/8/4 21:16
 */
@Slf4j
public class ErrorHandle {

    public ErrorHandle() {
    }

    public static void businessExceptionHandle(BaseResult result, String logMessage, BusinessException e) {
        log.warn(logMessage, e);
        result.setCode(e.getCode());
        result.setMessage(e.getMessage());
    }

    public static void systemExceptionHandle(BaseResult result, String logMessage, Exception e) {
        log.warn(logMessage, e);
        result.setCode(-500);
        result.setMessage("系统异常");
    }
}
