package com.shmilys.common.constant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

public interface DateFormatConstant {
    /**
     * 英文简写如：2010
     */
    String FORMAT_Y = "yyyy";

    /**
     * 英文简写如：12:01
     */
    String FORMAT_HM = "HH:mm";

    /**
     * 英文简写如：1-12 12:01
     */
   String FORMAT_M_D_H_M = "MM-dd HH:mm";

    /**
     * 英文简写（默认）如：2010-12-01
     */
   String FORMAT_Y_M_D = "yyyy-MM-dd";

   String FORMAT_Y_M_D_SN = "yyyyMMdd";

    /**
     * 英文全称  如：2010-12-01 23:15
     */
    String FORMAT_Y_M_D_H_M = "yyyy-MM-dd HH:mm";

    /**
     * 英文全称  如：2010-12-01 23:15:06
     */
    String FORMAT_Y_M_D_H_M_S = "yyyy-MM-dd HH:mm:ss";

    /**
     * 精确到毫秒的完整时间    如：yyyy-MM-dd HH:mm:ss.S
     */
    String FORMAT_FULL = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 精确到毫秒的完整时间    如：yyyy-MM-dd HH:mm:ss.S
     */
   String FORMAT_FULL_SN = "yyyyMMddHHmmssS";


    /**
     * 中文简写  如：2010年12月01日
     */
    String FORMAT_Y_M_D_CN = "yyyy年MM月dd日";

    /**
     * 中文简写  如：2010年12月01日  12时
     */
   String FORMAT_Y_M_D_H_CN = "yyyy年MM月dd日 HH时";

    /**
     * 中文简写  如：2010年12月01日  12时12分
     */
   String FORMAT_Y_M_D_H_M_CN = "yyyy年MM月dd日 HH时mm分";

    /**
     * 中文全称  如：2010年12月01日  23时15分06秒
     */
   String FORMAT_Y_M_D_H_M_S_CN = "yyyy年MM月dd日  HH时mm分ss秒";

    /**
     * 精确到毫秒的完整中文时间
     */
   String FORMAT_FULL_CN = "yyyy年MM月dd日  HH时mm分ss秒SSS毫秒";

    Pattern PATTERN = Pattern
            .compile("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-9]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9]))?)))?$");

    Calendar INSTANCE = Calendar.getInstance();
    static int getYear(){
    return INSTANCE.get(Calendar.YEAR);
    }
    static int getMonth(){
    return INSTANCE.get(Calendar.MONTH);
    }
    static int getDay(){
    return INSTANCE.get(Calendar.DAY_OF_MONTH);
    }
    static int getHours(){
    return INSTANCE.get(Calendar.HOUR_OF_DAY);
    }
    static int getMinutes(){
    return INSTANCE.get(Calendar.MINUTE);
    }
    static int getSeconds(){
    return INSTANCE.get(Calendar.SECOND);
    }

   SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
   SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("HH:mm",Locale.CHINA);
}
