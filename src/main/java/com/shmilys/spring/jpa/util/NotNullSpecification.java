package com.shmilys.spring.jpa.util;
import javax.persistence.criteria.*;

/**
 * 不为空动态查询条件
 */
public class NotNullSpecification<T> extends AbstractSpecification<T,Object> {

    public NotNullSpecification(String attrName) {
        super(attrName, null, AbstractSpecification.LOGICAL_OPERATOR_CUSTOM);
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Path<Object> path = SpecificationHelper.getPath(root, attrName);

        return cb.isNotNull(path);
    }
}