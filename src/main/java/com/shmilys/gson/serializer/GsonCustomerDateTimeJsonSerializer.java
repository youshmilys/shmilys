package com.shmilys.gson.serializer;

import cn.hutool.core.date.DateTime;
import com.google.gson.*;
import com.shmilys.tools.reflect.time.StringSmallUtils;

import java.lang.reflect.Type;

public class GsonCustomerDateTimeJsonSerializer  implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

    @Override
    public JsonElement serialize(DateTime dateTime, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(StringSmallUtils.dateTimeToSpecialString(dateTime));
    }

    @Override
    public DateTime deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return StringSmallUtils.specialStringToDateTime(jsonElement.getAsString());
    }

}
