/**
 * Copyright (c) 2015-2016, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.shmilys.tools.time;

import cn.hutool.core.date.DateTime;
import com.shmilys.common.constant.DateFormatConstant;
import com.shmilys.tools.format.MessageFormatUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class DateUtil {

    /**
     * 获取YYYY格式
     */
    public static String getYear() {
        return formatDate(new Date(), DateFormatConstant.FORMAT_Y);
    }

    /**
     * 获取YYYY格式
     */
    public static String getYear(Date date) {
        return formatDate(date,  DateFormatConstant.FORMAT_Y);
    }

    /**
     * 获取YYYY-MM-DD格式
     */
    public static String getDay() {
        return formatDate(new Date(), DateFormatConstant.FORMAT_Y_M_D);
    }

    /**
     * 获取YYYY-MM-DD格式
     */
    public static String getDay(Date date) {
        return formatDate(date,  DateFormatConstant.FORMAT_Y_M_D);
    }

    /**
     * 获取YYYYMMDD格式
     */
    public static String getDays() {
        return formatDate(new Date(), DateFormatConstant.FORMAT_Y_M_D_SN);
    }

    /**
     * 获取YYYYMMDD格式
     */
    public static String getDays(Date date) {
        return formatDate(date, DateFormatConstant.FORMAT_Y_M_D_SN);
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     */
    public static String getTime() {
        return formatDate(new Date(), DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss.SSS格式
     */
    public static String getMsTime() {
        return formatDate(new Date(),  DateFormatConstant.FORMAT_FULL);
    }

    /**
     * 获取YYYYMMDDHHmmss格式
     */
    public static String getAllTime() {
        return formatDate(new Date(), DateFormatConstant.FORMAT_FULL_SN);
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     */
    public static String getTime(Date date) {
        return formatDate(date, DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }

    public static String getTimeUntilMinute(Date date) {
        return formatDate(date, DateFormatConstant.FORMAT_Y_M_D_H_M);
    }

    public static String formatDate(Date date, String pattern) {
        String formatDate = null;
        if (StringUtils.isNotBlank(pattern)) {
            formatDate = DateFormatUtils.format(date, pattern);
        } else {
            formatDate = DateFormatUtils.format(date, DateFormatConstant.FORMAT_Y_M_D);
        }
        return formatDate;
    }

    /**
     * 日期比较，如果s>=e 返回true 否则返回false)
     *
     * @author luguosui
     */
    public static boolean compareDate(String s, String e) {
        if (parseDate(s) == null || parseDate(e) == null) {
            return false;
        }
        return parseDate(s).getTime() >= parseDate(e).getTime();
    }

    /**
     * 格式化日期
     */
    public static Date parseDate(String date) {
        return parse(date, DateFormatConstant.FORMAT_Y_M_D);
    }

    /**
     * 格式化日期
     */
    public static Date parseTimeMinutes(String date) {
        return parse(date, DateFormatConstant.FORMAT_Y_M_D_H_M);
    }

    /**
     * 格式化日期
     */
    public static Date parseTime(String date) {
        return parse(date, DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }

    /**
     * 格式化日期
     */
    public static Date parse(String date, String pattern) {
        try {
            return DateUtils.parseDate(date, pattern);
        } catch (ParseException e) {
            log.error(MessageFormatUtils.msgFormat("在com.shmilys.tools.time.DateUtil 方法 parse() 格式化日期失败，错误原因 : {}", e));
            return null;
        }
    }

    /**
     * 格式化日期
     */
    public static String format(Date date, String pattern) {
        return DateFormatUtils.format(date, pattern);
    }

    /**
     * 把日期转换为Timestamp
     */
    public static Timestamp format(Date date) {
        return new Timestamp(date.getTime());
    }

    /**
     * 校验日期是否合法
     */
    public static boolean isValidDate(String s) {
        return parse(s, "yyyy-MM-dd HH:mm:ss") != null;
    }

    /**
     * 校验日期是否合法
     */
    public static boolean isValidDate(String s, String pattern) {
        return parse(s, pattern) != null;
    }

    public static int getDiffYear(String startTime, String endTime) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int years = (int) (((fmt.parse(endTime).getTime() - fmt.parse(
                    startTime).getTime()) / (1000 * 60 * 60 * 24)) / 365);
            return years;
        } catch (Exception e) {
            log.error(MessageFormatUtils.msgFormat("在com.shmilys.tools.time.DateUtil 方法 getDiffYear() 传入参数的值的 格式不正确，错误原因 : {}", e));
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return 0;
        }
    }

    /**
     * <li>功能描述：时间相减得到天数
     */
    public static Long getDaySub(String beginDateStr, String endDateStr) {
        long day;
        SimpleDateFormat format = new SimpleDateFormat(DateFormatConstant.FORMAT_Y_M_D);
        Date beginDate = null;
        Date endDate = null;

        try {
            beginDate = format.parse(beginDateStr);
            endDate = format.parse(endDateStr);
        } catch (ParseException e) {
            log.error(MessageFormatUtils.msgFormat("在com.shmilys.tools.time.DateUtil 方法 getDaySub() 传入参数的值的 格式不正确，错误原因 : {}", e));
            return null;
        }
        day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
        log.info(MessageFormatUtils.msgFormat("相隔的天数 : {}",day));
        return day;
    }

    /**
     * 得到n天之后的日期
     */
    public static String getAfterDayDate(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormatConstant.FORMAT_Y_M_D_H_M_S);
        String time;
        try {
            time = sdf.format(date);
        }catch (Exception e){
            log.error(MessageFormatUtils.msgFormat("在com.shmilys.tools.time.DateUtil 方法 getAfterDayDate() 传入参数的值的 格式不正确，错误原因 : {}", e));
            return null;
        }
        return time;
    }

    /**
     * 得到n天之后是周几
     */
    public static String getAfterDayWeek(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String time;
        try {
            time = sdf.format(date);
        }catch (Exception e){
            log.error(MessageFormatUtils.msgFormat("在com.shmilys.tools.time.DateUtil 方法 getAfterDayWeek() 传入参数的值的 格式不正确，错误原因 : {}", e));
            return null;
        }
        return time;
    }
    
    /**
     * 将日期(yyyy-MM-dd)中的年，月，日单独提取出来
     */
    public static Map<String,Object> splitDate(String dateStr){
    	Map<String,Object> map = new HashMap<>();
    	String[] arr = dateStr.split("-");
    	map.put("year", arr[0]);
    	map.put("month", arr[1]);
    	map.put("day", arr[2]);
    	return map;
    }

    /**
     * 获取年月字符串(格式:yyyy/MM)
     * @param date
     * @return
     */
    public static String getYearAndMonth(Date date){
        if(date == null)
        {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
        String time;
        try {
            time = sdf.format(date);
        }catch (Exception e){
            log.error(MessageFormatUtils.msgFormat("在com.shmilys.tools.time.DateUtil 方法 getAfterDayWeek() 传入参数的值的 格式不正确，错误原因 : {}", e));
            return null;
        }
        return time;
    }


    /**
     * 两个时间相差距离多少天多少小时多少分多少秒
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00
     * @return String 返回值为：xx天xx小时xx分xx秒
     */
    public static String getDistanceTime(String str1, String str2) {
        DateFormat df = new SimpleDateFormat(DateFormatConstant.FORMAT_Y_M_D_H_M_S);
        Date one;
        Date two;
        long day;
        long hour;
        long min;
        try {
            one = df.parse(str1);
            two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff ;
            if(time1<time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
        } catch (ParseException e) {
            log.error(MessageFormatUtils.msgFormat("在com.shmilys.tools.time.DateUtil 方法 getDistanceTime() 传入参数的值的 格式不正确，错误原因 : {}", e));
            return null;
        }
        return day + "天 " + hour + "小时" + min + "分";
    }


    public static String getWeekOfDate(Date date) {
        String[] weekDays = { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        return weekDays[w];
    }



    /**
     * 获取本周的日期
     */
    public static List<String> getWeekDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> str = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DAY_OF_WEEK, -1);
        }
        Date[] dates = new Date[7];
        for (int i = 0; i < dates.length; i++) {
            dates[i] = calendar.getTime();
            calendar.add(Calendar.DATE, 1);
        }
        for (Date date : dates) {
            str.add(sdf.format(date));
        }
        return str;
    }

    /**
     * 根据当前周日的日期获取下周的日期
     */
    public static List<String> getNextWeekDay(Date date, int day) {
        List<String> days = new ArrayList<>();
        try {
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -1);
//            cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
//            cal.add(Calendar.WEEK_OF_YEAR, 1);


            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            //根据日期获取下周日期可修改此行代码
            //这里使用的是当前周日的日期
            calendar.add(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            if (Calendar.SUNDAY == calendar.get(Calendar.DAY_OF_WEEK)) {
                calendar.add(Calendar.DAY_OF_MONTH, -1);
            }

            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            calendar.add(
                    Calendar.DATE,
                    calendar.getFirstDayOfWeek()
                            - calendar.get(Calendar.DAY_OF_WEEK));

            for (int i = 0; i < day; i++) {
                days.add(DateTime.of(calendar.getTime()).toString("yyyy-MM-dd"));
                calendar.add(Calendar.DATE, 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return days;
    }


    /**
     * 获取当前日期的下周的所有日期集合(排斥了星期六星期天)
     * @return
     */
    public static List<String> getNextWeekDateList(Date date){
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);

        Calendar cal2 =Calendar.getInstance();
        cal2.setTime(date);

        // 获得当前日期是一个星期的第几天
        int dayWeek = cal1.get(Calendar.DAY_OF_WEEK);
        if(dayWeek == 1){
            cal1.add(Calendar.DAY_OF_MONTH, 1);
            cal2.add(Calendar.DAY_OF_MONTH, 7);
        } else {
            cal1.add(Calendar.DAY_OF_MONTH, 1 - dayWeek + 8);
            cal2.add(Calendar.DAY_OF_MONTH, 1 - dayWeek + 14);
        }

        Calendar cStart = Calendar.getInstance();
        cStart.setTime(cal1.getTime());

        List<String> dateList = new ArrayList<>();
        //别忘了，把起始日期加上
        dateList.add(DateTime.of(cal1.getTime()).toString("yyyy-MM-dd"));
        // 此日期是否在指定日期之后
        while (cal2.getTime().after(cStart.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            cStart.add(Calendar.DAY_OF_MONTH, 1);
            DateTime dateTime = DateTime.of(cStart.getTime());
            if (!dateTime.isWeekend()){
                dateList.add(dateTime.toString("yyyy-MM-dd"));
            }
        }
        return dateList;
    }


    /**
     * 当前时间的 上周一
     * @param date
     * @return
     */
    public static Date geLastWeekMonday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getThisWeekMonday(date));
        cal.add(Calendar.DATE, -7);
        return cal.getTime();
    }

    /**
     *  当前时间的 本周一
     * @param date
     * @return
     */
    public static Date getThisWeekMonday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 获得当前日期是一个星期的第几天
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        return cal.getTime();
    }

//    public static void main(String[] args) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        try {
//            Date date = sdf.parse("2020-12-27");
//            System.out.println("今天是" + sdf.format(date));
//            System.out.println("上周一" + sdf.format(geLastWeekMonday(date)));
//            System.out.println("本周一" + sdf.format(getThisWeekMonday(date)));
//            System.out.println("下周一" + sdf.format(getNextWeekMonday(date)));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     *  当前时间的 下周一
     * @param date
     * @return
     */
    public static Date getNextWeekMonday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getThisWeekMonday(date));
        cal.add(Calendar.DATE, 7);
        return cal.getTime();
    }
}
