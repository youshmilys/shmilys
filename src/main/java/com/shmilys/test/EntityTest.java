package com.shmilys.test;

import lombok.Data;

import java.util.Date;

@Data
public class EntityTest {

    private Integer id;

    private String name;

    private Integer age;

    private Date date;
}
