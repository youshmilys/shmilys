package com.shmilys.tools.verify;

import com.shmilys.tools.format.MessageFormatUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
public class VerifyUtlis {

    public final static Pattern PATTERN = Pattern.compile("^[+]?([0-9]+(.[0-9]{1,2})?)$");

    /**
     * 两位小数金额校验
     */
    public static boolean judgeTwoDecimal(Object obj) {
        boolean flag = false;
        try {
            if (obj != null) {
                String source = obj.toString();
                // 判断是否是整数或者是携带一位或者两位的小数
                if (PATTERN.matcher(source).matches()) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            log.error(MessageFormatUtils.msgFormat("两位小数金额校验异常 ---> 异常内容: {0}", e.getMessage()));
            log.error(MessageFormatUtils.msgFormat("参数值为：{0}", obj));
        }
        return flag;
    }


}
