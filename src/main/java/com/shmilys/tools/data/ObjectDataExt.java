package com.shmilys.tools.data;

import com.shmilys.common.base.ObjectData;
import lombok.experimental.Delegate;

/**
 * @Description objectData的扩展类
 * @Author quzf
 * @Create 2018-11-05 7:13 PM
 */
public class ObjectDataExt {
    @Delegate
    private  ObjectData objectData;

    private ObjectDataExt(ObjectData objectData) {
        this.objectData = objectData;
    }

    public ObjectData get() {
        return objectData;
    }

    public static ObjectDataExt of(ObjectData objectData) {
        return new ObjectDataExt(objectData);
    }

    public <T> T getOrDefault(String key, Class<T> clazz, T defaultObj) {
        T t = this.get(key, clazz);
        if (t == null) {
            return defaultObj;
        }
        return t;
    }

}
