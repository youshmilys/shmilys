package com.shmilys.tools.str;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StrSplitUtil {

    public static final String COMMA = ",";

    public static final String SEMICOLON = ";";

    public static List<String> parseStrToStrList(String str,String sign){
        if (!StringUtils.isNoneBlank(str,sign)){
            return null;
        }
        return Arrays.asList(StrUtil.split(str, sign));
    }


    public static String parseStrListToStr(List<String> sourceList, String sign){
        if (CollectionUtils.isEmpty(sourceList) && StrUtil.isEmpty(sign)){
            return null;
        }
        return String.join(sign, sourceList);
    }

//    public static void main(String[] args) {
//        List<String> list1 = new ArrayList<>();
//        list1.add("1");
//        list1.add("2");
//        list1.add("3");
//        list1.add("4");
//        list1.add("5");
//
//        List<String> list2 = new ArrayList<>();
//        list2.add("2");
//        list2.add("3");
//        list2.add("6");
//        list2.add("7");
//
//        // 交集
//        List<String> intersection = list1.stream().filter(item -> list2.contains(item)).collect(Collectors.toList());
//        System.out.println("---交集 intersection---");
//        intersection.parallelStream().forEach(System.out::println);
//
//        // 差集 (list1 - list2)
//        List<String> reduce1 = list1.stream().filter(item -> !list2.contains(item)).collect(Collectors.toList());
//        System.out.println("---差集 reduce1 (list1 - list2)---");
//        reduce1.parallelStream().forEach(System.out::println);
//
//        // 差集 (list2 - list1)
//        List<String> reduce2 = list2.stream().filter(item -> !list1.contains(item)).collect(Collectors.toList());
//        System.out.println("---差集 reduce2 (list2 - list1)---");
//        reduce2.parallelStream().forEach(System.out::println);
//
//        // 并集
//        List<String> listAll = list1.parallelStream().collect(Collectors.toList());
//        List<String> listAll2 = list2.parallelStream().collect(Collectors.toList());
//        listAll.addAll(listAll2);
//        System.out.println("---并集 listAll---");
//        listAll.parallelStream().forEachOrdered(System.out::println);
//
//        // 去重并集
//        List<String> listAllDistinct = listAll.stream().distinct().collect(Collectors.toList());
//        System.out.println("---得到去重并集 listAllDistinct---");
//        listAllDistinct.parallelStream().forEachOrdered(System.out::println);
//
//    }


    public static void main(String[] args) {
        List<String> aList = new ArrayList<>();
        aList.add("nihao");
        aList.add("nibuhao");
        aList.add("woaini");
        aList.add("wobuaini");

        List<String> bList = new ArrayList<>();
        bList.add("woxiangni");
        bList.add("wobuxiangni");
        bList.add("nihao");

        String s = parseStrListToStr(aList, StrSplitUtil.COMMA);
        System.out.println("s = " + s);

    }



}
