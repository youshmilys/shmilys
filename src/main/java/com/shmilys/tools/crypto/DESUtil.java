package com.shmilys.tools.crypto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.SecureRandom;


public class DESUtil {
    private static Logger logger = LoggerFactory.getLogger(DESUtil.class);

    //密码，长度要是8的倍数    注意此处为简单密码  简单应用 要求不高时可用此密码
    private static String password = "19880307";

    //测试
    public static void main(String args[]) {
        //待加密内容
        String str = "jingdata-micuncang";

        String result = DESUtil.encrypt(str);
        System.out.println("加密后：" + result);
        String decryResult = DESUtil.decryptor(result);
        System.out.println("解密后：" + decryResult);
    }

    /**
     * @Method: encrypt
     * @Description: 加密数据
     * @date 2016年7月26日
     */
    public static String encrypt(String data) {  //对string进行BASE64Encoder转换
        byte[] bt = encryptByKey(data.getBytes(), password);
        BASE64Encoder base64en = new BASE64Encoder();
        String strs = base64en.encode(bt);
        return strs;
    }

    /**
     * @Method: encrypt
     * @Description: 解密数据
     * @date 2016年7月26日
     */
    public static String decryptor(String data) {  //对string进行BASE64Encoder转换
        try {
            BASE64Decoder base64en = new BASE64Decoder();
            byte[] bt = decrypt(base64en.decodeBuffer(data), password);
            String strs = new String(bt);
            return strs;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 加密
     */
    private static byte[] encryptByKey(byte[] datasource, String key) {
        try {
            SecureRandom random = new SecureRandom();

            DESKeySpec desKey = new DESKeySpec(key.getBytes());
            //创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            //现在，获取数据并加密
            //正式执行加密操作
            return cipher.doFinal(datasource);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     */
    private static byte[] decrypt(byte[] src, String key) throws Exception {
        // DES算法要求有一个可信任的随机数源
        SecureRandom random = new SecureRandom();
        // 创建一个DESKeySpec对象
        DESKeySpec desKey = new DESKeySpec(key.getBytes());
        // 创建一个密匙工厂
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        // 将DESKeySpec对象转换成SecretKey对象
        SecretKey securekey = keyFactory.generateSecret(desKey);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance("DES");
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, random);
        // 真正开始解密操作
        return cipher.doFinal(src);
    }
}
