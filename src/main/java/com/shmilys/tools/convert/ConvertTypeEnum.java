package com.shmilys.tools.convert;

import com.alibaba.fastjson.JSON;
import com.shmilys.tools.time.DateUtil;

/**
 * @author: jianxiao
 * @date: 2020/11/18 16:58
 */
public enum ConvertTypeEnum {
    /**
     * 数据类型
     */
    TYPE_STRING("String"){
        @Override
        public <T> T getConvertType(Object result) {
            return (T) Convert.toStr(result);
        }
    },
    TYPE_INTEGER("Integer"){
        @Override
        public <T> T getConvertType(Object result) {
            return (T) Convert.toInt(result);
        }
    },TYPE_LONG("Long"){
        @Override
        public <T> T getConvertType(Object result) {
            return (T) Convert.toLong(result);
        }
    },TYPE_DOUBLE("Double"){
        @Override
        public <T> T getConvertType(Object result) {
            return (T) Convert.toDouble(result);
        }
    },TYPE_BIG_DECIMAL("BigDecimal"){
        @Override
        public <T> T getConvertType(Object result) {
            return (T) Convert.toBigDecimal(result);
        }
    },TYPE_DATE("Date"){
        @Override
        public <T> T getConvertType(Object result) {
            return (T) DateUtil.parseDate(result.toString());
        }
    },TYPE_DATE_TIME("Timestamp"){
        @Override
        public <T> T getConvertType(Object result) {
            return  (T) DateUtil.format(ConvertTypeEnum.TYPE_DATE.getConvertType(result));
        }
    },TYPE_LIST("List"){
        @Override
        public <T> T getConvertType(Object result) {
            String str;
            if (result instanceof String) {
                str = (String) result;
            } else {
                str = JSON.toJSONString(value);
            }
            return (T) JSON.parseArray(result.toString(), Object.class);
        }
    };

    String value;

    ConvertTypeEnum(String value) {
        this.value = value;
    }

    public abstract <T> T getConvertType(Object result);
}
