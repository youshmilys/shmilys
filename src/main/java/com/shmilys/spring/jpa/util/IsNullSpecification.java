package com.shmilys.spring.jpa.util;

import javax.persistence.criteria.*;

/**
 *
 */
public class IsNullSpecification<T> extends AbstractSpecification<T,Object> {
    public IsNullSpecification(String attrName) {
        super(attrName, null, AbstractSpecification.LOGICAL_OPERATOR_CUSTOM);
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Path<Object> path = SpecificationHelper.getPath(root, attrName);

        return cb.isNull(path);
    }
}