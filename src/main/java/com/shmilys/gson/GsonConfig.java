package com.shmilys.gson;

import cn.hutool.core.date.DateTime;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shmilys.gson.serializer.GsonCustomerDateJsonSerializer;
import com.shmilys.gson.serializer.GsonCustomerDateTimeJsonSerializer;
import com.shmilys.gson.serializer.GsonCustomerLocalDateTimeJsonSerializer;
import com.shmilys.gson.serializer.GsonCustomerTimestampJsonSerializer;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

public class GsonConfig {

    private static final Gson GSON;

    static {
        // 当使用GsonBuilder方式时属性为空的时候输出来的json字符串是有键值key的,显示形式是"key":null，而直接new出来的就没有"key":null的
        GSON = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation() //不导出实体中没有用@Expose注解的属性
//                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE) //将属性的首字母大写
//                .setDateFormat("yyyy-MM-dd HH:mm:ss")   //日期格式转换
                .serializeNulls() //当需要序列化的值为空时，采用null映射，否则会把该字段省略
                .setPrettyPrinting()//将结果进行格式化
                //.setFieldNamingStrategy(f -> StrUtil.toCamelCase(f.getName())) //实体的下划线转驼峰
                .registerTypeAdapter(DateTime.class, new GsonCustomerDateTimeJsonSerializer())  //自定义dateTime序列化与反序列化日期
                .registerTypeAdapter(Date.class, new GsonCustomerDateJsonSerializer())          //自定义date序列化与反序列化日期
                .registerTypeAdapter(Timestamp.class, new GsonCustomerTimestampJsonSerializer()) //自定义Timestamp序列化与反序列化日期
                .registerTypeAdapter(LocalDateTime.class, new GsonCustomerLocalDateTimeJsonSerializer())//自定义LocalDateTime序列化与反序列化日期
                .create();
    }

    public static Gson getGson(){
        return GSON;
    }
}
