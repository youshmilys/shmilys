package com.shmilys.tree;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 树节点,需要拓展属性时，请采用继承的方式
 *
 */
@Data
public class TreeNode {
    private String name;
    private String id;
    private String pId;
    private String parentsPath;

    private Boolean checked = false;
    private List<TreeNode> children;

    public void addChild(TreeNode child) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
    }

}
