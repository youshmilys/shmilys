package com.shmilys.test;

import cn.hutool.core.date.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {


    public static void main(String[] args) {
        List<EntityTest> entityTestList = new ArrayList<>();
        EntityTest test1 = new EntityTest();
        test1.setId(1);
        test1.setAge(19);
        test1.setName("韦海超1");
        test1.setDate(DateTime.now());
        entityTestList.add(test1);

        EntityTest test2 = new EntityTest();
        test2.setId(2);
        test2.setAge(12);
        test2.setName("韦海超2");
        test2.setDate(DateTime.now());
        entityTestList.add(test2);

        EntityTest test3 = new EntityTest();
        test3.setId(3);
        test3.setAge(13);
        test3.setName("韦海超3");
        test3.setDate(DateTime.now());
        entityTestList.add(test3);




        List<Map<String,Object>> mapList = new ArrayList<>();
        Map<String,Object> map1 = new HashMap<>();
        map1.put("stu_id",1);
        map1.put("stu_name","韦海超");
        map1.put("stu_age",11);
        map1.put("stu_date",DateTime.now());
        mapList.add(map1);

        Map<String,Object> map2 = new HashMap<>();
        map2.put("stu_id",12);
        map2.put("stu_name","韦海超2");
        map2.put("stu_age",112);
        map2.put("stu_date",DateTime.now());
        mapList.add(map2);

        Map<String,Object> map3 = new HashMap<>();
        map3.put("stu_id",13);
        map3.put("stu_name","韦海超3");
        map3.put("stu_age",113);
        map3.put("stu_date",DateTime.now());
        mapList.add(map3);


        List<Object> objectList = new ArrayList<>();
        objectList.add(map1);
        objectList.add(map2);
        objectList.add(map3);

//        BigDecimalUtils.














//        EntityToTest entityToTest = new EntityToTest();
//        entityToTest.setStu_id(1);
//        entityToTest.setStu_date(new DateTime());
//        entityToTest.setStu_name("韦海超");
//        entityToTest.setStu_age(20);
//        System.out.println(objectToGson(entityToTest));

        //        List<Map<String,Object>> mapList = new ArrayList<>();
//        Map<String,Object> map1 = new HashMap<>();
//        map1.put("id",1);
//        map1.put("name","韦海超");
//        map1.put("age",11);
//        map1.put("date",DateTime.now());
//        mapList.add(map1);
//
//        Map<String,Object> map2 = new HashMap<>();
//        map2.put("id",12);
//        map2.put("name","韦海超2");
//        map2.put("age",112);
//        map2.put("date",DateTime.now());
//        mapList.add(map2);
//
//        Map<String,Object> map3 = new HashMap<>();
//        map3.put("id",13);
//        map3.put("name","韦海超3");
//        map3.put("age",113);
//        map3.put("date",DateTime.now());
//        mapList.add(map3);
        //System.out.println(objectToGson(map3));
        //System.out.println(gsonToList(objectToGson(map1),EntityTest.class));
        // System.out.println(gsonToBean(objectToGson(test3),EntityTest.class));
//
//        System.out.println("test1 = " + test1);
//
//        String json = gson.toJson(test1);
//        System.out.println("序列化:"+json);
//
//        test1 = gson.fromJson(json,EntityTest.class);
//        System.out.println("反序列化：\n"+test1);
    }
}
