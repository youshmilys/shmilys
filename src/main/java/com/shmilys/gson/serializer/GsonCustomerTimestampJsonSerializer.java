package com.shmilys.gson.serializer;

import com.google.gson.*;
import com.shmilys.tools.reflect.time.StringSmallUtils;

import java.lang.reflect.Type;
import java.sql.Timestamp;

public class GsonCustomerTimestampJsonSerializer implements JsonSerializer<Timestamp>, JsonDeserializer<Timestamp> {


    @Override
    public Timestamp deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return StringSmallUtils.specialStringToTimestamp(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(Timestamp timestamp, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(StringSmallUtils.timestampToSpecialString(timestamp));
    }
}
