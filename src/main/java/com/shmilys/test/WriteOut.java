package com.shmilys.test;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class WriteOut {


    /**
     * 自定义映射类 并获取获取查询结果集
     * @param clz 自定义映射类
     * @param listResult 查询的结果
     * @param <T> 自定义类型 (限于Java类)
     * @return 结果映射类。
     */
    public static <T> List<T> getQueryResult(Class<T> clz, List<Map<String, Object>> listResult) {
        List<T> list = new ArrayList<>();
        listResult.forEach( data -> {
            //Map转换为Bean对象  返回结果值  要转换的bean对象  空也不会报错
            list.add( BeanUtil.mapToBeanIgnoreCase( data, clz, true ) );
        } );
        return list;
    }

    /**
     * 如果Integer数组中有空值 替换空赋值0
     * @param arr  检查Integer[]数组
     * @return  返回检查后数组
     */
    public static Integer[] getIntArrayData(Integer[] arr ) {
        Stream<Integer> language = Stream.of( arr );
        List<Integer> array = new ArrayList<>();
        language.forEach( data -> {
            if( data == null ){
                data = 0;
            }
            array.add( data ) ;
        });
        return ArrayUtil.toArray( array , Integer.class);
    }

    /**
     *  获取当前天与偏移天7天(-6)的所有日期
     * @return
     */
    public static List<String> getNowAndOffsetDayAll() {
        List<String> list = new ArrayList<>();
        for( int i = -6; i <= 0 ; i++ ){
            DateTime dateTimes = DateUtil.offsetDay( DateUtil.date() ,i );
            list.add( DateUtil.format( dateTimes, "MM/dd") );
        }
        return list;
    }

    /**
     *  获取当前天与偏移天数的日期
     *  当前日期是0 偏移后日期是1
     * @param dateFormat 时间格式
     * @param offsetDays 偏移天
     * @return  返回当前日期与偏移后到达的日期
     */
    public static String[] getNowAndOffset(String dateFormat , Integer offsetDays ){
        String[] array = new String[2];
        array[0] = DateUtil.format(DateUtil.date(), dateFormat);
        DateTime dateTime = DateUtil.offsetDay( DateUtil.date() , offsetDays);
        array[1] = DateUtil.format( dateTime, dateFormat);
        return array;
    }

    /**
     * 获取某个数值的相反数
     * @param number
     * @return
     */
    public static int opposite(int number) {
        return Math.negateExact(number);
    }

    /**
     * BigDecimal 转换万元
     */
    public static String convertValue(BigDecimal value ) {
        //转换万元
        BigDecimal calculateValue  = value.divide( new BigDecimal( 10000 ) );
        //保留两位小数
        DecimalFormat format = new DecimalFormat("0.00");
        //四舍五入
        format.setRoundingMode( RoundingMode.HALF_UP );
        return format.format( calculateValue  );
    }

    /**
     * 用于解析以; 英文分号分割 组成的字符串
     * @param photos
     * @return
     */
    public  static String[] getPhotos(String photos ) {
        String[] result = null;
        if( StrUtil.isNotBlank( photos ) ){
            //以逗号分割，得出的数据存到 result 里面
             result = photos.split(";");
        }
        return result;
    }

    /**
     * 用于多个文件上传，文件的URL用英文分号;
     * @param imageUrlGroup
     * @return
     */
    public static String getFileUploadPath(String[] imageUrlGroup ) {
        StringBuilder urlHttps = new StringBuilder();
        if( ArrayUtil.isNotEmpty(imageUrlGroup)) {
            for(String img : imageUrlGroup) {
                if(img.contains("https://")){
                    urlHttps.append(img).append(";");
                }else {
                    urlHttps.append("https://").append(img).append(";");
                }
            }
            //去除最后分号
            if( imageUrlGroup.length != 0 ) {
                urlHttps = new StringBuilder(urlHttps.substring(0 , urlHttps.length() - 1));
            }
        }
        return urlHttps.toString();
    }

    /**
     * 数组 多个图片路径前缀加上https://
     * @param ImageUrlGroup
     * @return
     */
    public static String addImageUrlGroupPrefixHttps(String[] ImageUrlGroup){
        String httpUrl = "";
        if(ArrayUtil.isNotEmpty(ImageUrlGroup)){
             httpUrl = getFileUploadPath(ImageUrlGroup);
        }
        return httpUrl;
    }

    /**
     * 单个 图片路径加前缀 https://
     * @param imageUrlSingle
     * @return
     */
    public static String addImageUrlSinglePrefixHttps(String imageUrlSingle){
        StringBuilder httpUrl = new StringBuilder();
        if(StrUtil.isNotBlank(imageUrlSingle)){
            if(imageUrlSingle.contains("https://")){
                httpUrl.append(imageUrlSingle);
            }else {
                httpUrl.append("https://") .append(imageUrlSingle);
            }
        }
        return httpUrl.toString();
    }


//    /**
//     * 截取文件后缀 与 文件名称 (name.jpg)
//     * @param fileUrl   文件路径
//     * @return  返回数组 0是文件后缀 1是文件前缀名称
//     */
//    public static String[] interceptName(String fileUrl){
//        String name = fileUrl.substring(fileUrl.lastIndexOf ("/") + 1); //得到 name.jpg 等后缀和名字
//        String[] fileName = new String[2];//jpg文件后缀  //name文件前缀名称
//        if(StrUtil.isNotBlank(name) && !"".equals(name.trim())){
//            fileName[0] = name.substring(name.lastIndexOf(".") +1);      //jpg文件后缀
//            fileName[1] = name.substring(0,name.indexOf("."));               //name文件前缀名称
//        } else {
//            throw new BadRequestException("资源文件截取失败!");
//        }
//        return fileName;
//    }

//
//    /**
//     * 获取类中的属性
//     * @param clazz 类
//     * @param object class对象
//     * @param id   id用于判断是新增还是修改
//     */
//    public static void getField(Class<?> clazz, Object object, Integer id) {
//
//        if (id == null){
//            Field fieldCreateTime = null;
//            Field fieldCreateBy = null;
//            try {
//                fieldCreateTime = clazz.getDeclaredField("createTime");
//                fieldCreateBy = clazz.getDeclaredField("createBy");
//                //获取到了字段
//                setField (object, fieldCreateTime, fieldCreateBy);
//            } catch ( NoSuchFieldException | IllegalAccessException e) {
//                e.printStackTrace ();
//            }
//        }else {
//            Field fieldUpdateTime = null;
//            Field fieldUpdateBy = null;
//            try {
//                fieldUpdateTime = clazz.getDeclaredField("updateTime");
//                fieldUpdateBy = clazz.getDeclaredField("updateBy");
//                //获取到了字段
//                setField (object, fieldUpdateTime, fieldUpdateBy);
//            } catch ( NoSuchFieldException | IllegalAccessException e) {
//                e.printStackTrace ();
//            }
//        }
//
//    }
//
//    /**
//     * 修改获取到的字段
//     * @param object 对象
//     * @param fieldTime 属性
//     * @param fieldName 属性
//     * @throws IllegalAccessException
//     */
//    private static void setField (Object object, Field fieldTime, Field fieldName) throws IllegalAccessException {
//        fieldTime.setAccessible(true);
//        fieldName.setAccessible(true);
//        fieldTime.set(object, new Timestamp(System.currentTimeMillis()));
//        fieldName.set(object, SecurityUtils.getCurrentUsername());
//    }

}
