package com.shmilys.tools.reflect.time;

import cn.hutool.core.date.DateTime;
import com.shmilys.common.constant.DateFormatConstant;
import com.shmilys.tools.time.DateUtil;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class StringSmallUtils {


    public static void main(String[] args) {
        System.out.println("System.currentTimeMillis() = " + System.currentTimeMillis());
    }

    /**
     * LocalDate 时间类型格式转换为指定的String类型
     *
     * @param localDateTime
     * @return
     */
    public static String localDateTimeToSpecialString(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return localDateTime.format(DateTimeFormatter.ofPattern(DateFormatConstant.FORMAT_Y_M_D_H_M_S));
    }

    /**
     * LocalDate 指定的String类型转换为时间类型格式
     *
     * @param str
     * @return
     */
    public static LocalDateTime specialStringToLocalDateTime(String str) {
        return LocalDateTime.parse(str, DateTimeFormatter.ofPattern(DateFormatConstant.FORMAT_Y_M_D_H_M_S));
    }



    /**
     * Timestamp 时间类型格式转换为指定的String类型
     *
     * @param timestamp
     * @return
     */
    public static String timestampToSpecialString(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return DateUtil.format(timestamp, DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }

    /**
     * Timestamp 指定的String类型转换为时间类型格式
     *
     * @param str
     * @return
     */
    public static Timestamp specialStringToTimestamp(String str) {
        return Timestamp.valueOf(str);
    }

    /**
     * dateTime 时间类型格式转换为指定的String类型
     *
     * @param dateTime
     * @return
     */
    public static String dateTimeToSpecialString(DateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return DateUtil.format(dateTime, DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }


    /**
     * dateTime 指定的String类型转换为时间类型格式
     *
     * @param str
     * @return
     */
    public static DateTime specialStringToDateTime(String str) {
        return DateTime.of(str, DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }


    /**
     * date 时间类型格式转换为指定的String类型
     *
     * @param date
     * @return
     */
    public static String dateToSpecialString(Date date) {
        if (date == null) {
            return null;
        }
        return DateUtil.format(date, DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }


    /**
     * date 指定的String类型转换为时间类型格式
     *
     * @param str
     * @return
     */
    public static Date specialStringToDate(String str) {
        return DateUtil.parse(str, DateFormatConstant.FORMAT_Y_M_D_H_M_S);
    }

    /**
     * 判断字符串是否包含输入的字符串
     *
     * @param str
     * @param searchStr
     * @return
     */
    public static boolean contains(String str, String searchStr) {
        if (str == null || searchStr == null) {
            return false;
        }
        return str.contains(searchStr);
    }

    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return ((str == null) || (str.trim().isEmpty()));
    }
}

