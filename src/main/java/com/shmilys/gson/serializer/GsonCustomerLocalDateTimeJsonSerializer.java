package com.shmilys.gson.serializer;

import com.google.gson.*;
import com.shmilys.tools.reflect.time.StringSmallUtils;

import java.lang.reflect.Type;
import java.time.LocalDateTime;

public class GsonCustomerLocalDateTimeJsonSerializer implements JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {
    @Override
    public LocalDateTime deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return StringSmallUtils.specialStringToLocalDateTime(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(LocalDateTime localDateTime, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(StringSmallUtils.localDateTimeToSpecialString(localDateTime));
    }
}
