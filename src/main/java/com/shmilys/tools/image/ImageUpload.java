package com.shmilys.tools.image;

import lombok.Data;

/**
 * 用于图片添加、图片回显(比如：应用场景,点击编辑时从后端返回数据到前端显示)
 */
@Data
public class ImageUpload {
    /**
     * 文件名称
     */
    private String name;

    /**
     * 文件url
     */

    private String url;

    /**
     * 文件uid
     */

    private String uId;

    /**
     * 文件size
     */

    private String size;

    /**
     * 图片status
     */
    private String status;
}
