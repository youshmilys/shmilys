package com.shmilys.tools.str;

import org.apache.commons.lang3.ArrayUtils;

public class VerifyStrUtils {



    public static boolean isBlank(CharSequence cs) {
        int strLen;
        if (cs != null && (strLen = cs.length()) != 0) {
            for(int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }

    public static boolean isAllBlank(CharSequence... css) {
        if (!ArrayUtils.isEmpty(css)) {
            for (CharSequence cs : css) {
                if (isNotBlank(cs)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isNotBlank(CharSequence cs) {
        return !isBlank(cs);
    }
}
