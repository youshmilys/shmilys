package com.shmilys.test;

import lombok.Data;

import java.util.Date;

@Data
public class EntityToTest {
    private Integer stu_id;

    private String stu_name;

    private Integer stu_age;

    private Date stu_date;
}
