package com.shmilys.tools.custom.map;

import com.google.common.collect.Maps;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapUtils {

    /**
     * <h1> 根据多属性进行list转map分组</h1>
     * 可以自行修改,稍稍修改就可以达到集合里指定属性去重的效果
     *
     * @param list    要转换的集合
     * @param strings 转换后Map的key值,例如根据name属性分组,这里就填写name
     *                如果是要求name和age属性都相同的分为一组,这里就填name,age,更多属性同理
     *                key值默认为属性名的字符串连接,如果要指定的key名修改代码的20行和22行即可
     * @param <T>     集合里对象的泛型,Matrix
     * @return 根据属性分好组的Map
     */
    public static <T> Map<String, List<T>> listToMap(List<T> list, String... strings) {
        Map<String, List<T>> returnMap = new HashMap<>();
        try {
            for (T t : list) {
                StringBuilder stringBuffer = new StringBuilder();
                for (String s : strings) {
                    Field name1 = t.getClass().getDeclaredField(s);//通过反射获得私有属性
                    name1.setAccessible(true);
                    String key = name1.get(t).toString();//获得key值
                    stringBuffer.append(key);
                }
                String keyName = stringBuffer.toString();

                List<T> tempList = returnMap.get(keyName);
                if (tempList == null) {
                    tempList = new ArrayList<>();
                    tempList.add(t);
                    returnMap.put(keyName, tempList);
                } else {
                    tempList.add(t);
                }
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return returnMap;
    }

    public static Map<String, Object> beanToMap(Object obj) {
        return beanToMap(obj, null);
    }

    public static Map<String, Object> beanToMap(Object obj, Map<String, Object> map) {
        if (org.springframework.util.CollectionUtils.isEmpty(map)) {
            map = Maps.newHashMap();
        }
        if (obj == null) { return null; }
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            if (getValueByFieldName(fieldName, obj) != null) {
                map.put(fieldName, getValueByFieldName(fieldName, obj));
            }
        }
        return map;
    }

    private static Object getValueByFieldName(String fieldName,Object object) {
        String firstLetter = fieldName.substring(0, 1).toUpperCase();
        String getter = "get" + firstLetter + fieldName.substring(1);
        try {
            Method method = object.getClass().getMethod(getter);
            return method.invoke(object);
        } catch (Exception e) {
            return null;
        }
    }
//
//    private void otherTest() {
//        // 比较大小，值可以是null
//        String left = "18.2";
//        String right = "20";
//        String minNum = CompareUtil.compare(left, right) < 0 ? left : right;
//        System.out.println("compare:" + minNum);
//
//        // 保留5位小数点
//        double a = 18.54898145646132;
//        System.out.println(NumberUtil.decimalFormat("#.#####", a)); // 18.54898
//        double b = 18.50;
//        System.out.println(NumberUtil.decimalFormat("#.#####", b)); // 18.5
//    }
//
//    /**
//     * NullPointerException
//     * 空指针测试
//     */
//    private void nullTest() {
//        ObjectData data = new ObjectData();
//        ObjectData data2 = new ObjectData();
////        data2.set("children","我是children");
//        ObjectDataUtil.set(data2, "children", "我是children");
//
////        data2 = null;
////        data2 = ObjectDataUtil.set(data2,"children","我是children",true);
//
////        data.set("name","谢超");
////        DataUtil.set(data, "name", "谢超");
//        ObjectDataUtil.set(data, "sex", "男");
////        data.set("parent",data2);
//        ObjectDataUtil.set(data, "parent", data2);
////        data = null;
//        System.out.println("data:" + data);
//
//
//        // 不需要判断data是否为null，如果key值“name”不存在，可以自定义返回数据
//        Object res1 = ObjectDataUtil.get(data, "name", "我是默认返回对象");
//        System.out.println("res1:" + res1);
//
//        // 链式 获取
//        Object res2 = ObjectDataUtil.getKeys(data, "parent", "children");
//        System.out.println("res2:" + res2);
//
//        Object res3 = ObjectDataUtil.get(data, "xxx", null);
//        System.out.println("res3:" + res3);
//
//        String res4 = ObjectDataUtil.getT(data, "sex", String.class);
//        System.out.println("res4:" + res4);
//    }
//
//    /**
//     * StrUtil
//     * 字符串工具类，定义了一些常用的字符串操作方法。
//     */
//    private void strTest() {
//        //判断是否为空字符串
//        String str = "test";
//        StrUtil.isEmpty(str);
//        StrUtil.isNotEmpty(str);
//        //去除字符串的前后缀
//        StrUtil.removeSuffix("a.jpg", ".jpg");
//        StrUtil.removePrefix("a.jpg", "a.");
//        //格式化字符串
//        String template = "这只是个占位符:{}";
//        String str2 = StrUtil.format(template, "我是占位符");
//        System.out.println(str2);
//    }
//
//    /**
//     * NumberUtil
//     * 数字处理工具类，可用于各种类型数字的加减乘除操作及判断类型。
//     */
//    private void numberTest() {
//        double n1 = 1.234;
//        double n2 = 1.234;
//        double result;
//        //对float、double、BigDecimal做加减乘除操作
//        result = NumberUtil.add(n1, n2);
//        result = NumberUtil.sub(n1, n2);
//        result = NumberUtil.mul(n1, n2);
//        result = NumberUtil.div(n1, n2);
//        //保留两位小数
//        BigDecimal roundNum = NumberUtil.round(n1, 2);
//        String n3 = "1.234";
//        //判断是否为数字、整数、浮点数
//        NumberUtil.isNumber(n3);
//        NumberUtil.isInteger(n3);
//        NumberUtil.isDouble(n3);
//    }
//
//    /**
//     * DateUtil
//     * 日期时间工具类，定义了一些常用的日期时间操作方法。
//     */
//    private void dateTest() {
//        //Date、long、Calendar之间的相互转换
//        //当前时间
//        Date date = DateUtil.date();
//        //Calendar转Date
//        date = DateUtil.date(Calendar.getInstance());
//        //时间戳转Date
//        date = DateUtil.date(System.currentTimeMillis());
//        //自动识别格式转换
//        String dateStr = "2017-03-01";
//        date = DateUtil.parse(dateStr);
//        //自定义格式化转换
//        date = DateUtil.parse(dateStr, "yyyy-MM-dd");
//        //格式化输出日期
//        String format = DateUtil.format(date, "yyyy-MM-dd");
//        //获得年的部分
//        int year = DateUtil.year(date);
//        //获得月份，从0开始计数
//        int month = DateUtil.month(date);
//        //获取某天的开始、结束时间
//        Date beginOfDay = DateUtil.beginOfDay(date);
//        Date endOfDay = DateUtil.endOfDay(date);
//        //计算偏移后的日期时间
//        Date newDate = DateUtil.offset(date, DateField.DAY_OF_MONTH, 2);
//        //计算日期时间之间的偏移量
//        long betweenDay = DateUtil.between(date, newDate, DateUnit.DAY);
//    }
//
//    /**
//     * CollUtil
//     * 集合操作的工具类，定义了一些常用的集合操作。
//     */
//    private void collTest() {
//        //数组转换为列表
//        String[] array = new String[]{"a", "b", "c", "d", "e"};
//        List<String> list = CollUtil.newArrayList(array);
//        //join：数组转字符串时添加连接符号
//        String joinStr = CollUtil.join(list, ",");
//        //将以连接符号分隔的字符串再转换为列表
//        List<String> splitList = StrUtil.split(joinStr, ',');
//        //创建新的Map、Set、List
//        HashMap<Object, Object> newMap = CollUtil.newHashMap();
//        HashSet<Object> newHashSet = CollUtil.newHashSet();
//        ArrayList<Object> newList = CollUtil.newArrayList();
//        //判断列表是否为空
//        CollUtil.isEmpty(list);
//    }
//
//    /**
//     * MapUtil
//     * Map操作工具类，可用于创建Map对象及判断Map是否为空。
//     */
//    private void mapTest() {
//        //将多个键值对加入到Map中
//        Map<Object, Object> map = MapUtil.of(new String[][]{
//                {"key1", "value1"},
//                {"key2", "value2"},
//                {"key3", "value3"}
//        });
//        //判断Map是否为空
//        MapUtil.isEmpty(map);
//        MapUtil.isNotEmpty(map);
//    }
//
//    /**
//     * Convert
//     * 类型转换工具类，用于各种类型数据的转换。
//     */
//    private void convertTest() {
//        //转换为字符串
//        int a = 1;
//        String aStr = Convert.toStr(a);
//        //转换为指定类型数组
//        String[] b = {"1", "2", "3", "4"};
//        Integer[] bArr = Convert.toIntArray(b);
//        //转换为日期对象
//        String dateStr = "2017-05-06";
//        Date date = Convert.toDate(dateStr);
//        //转换为列表
//        String[] strArr = {"a", "b", "c", "d"};
//        List<String> strList = Convert.toList(String.class, strArr);
//    }
//
//
//    // 框架协议 代码逻辑demo
//    private void listMapTest() {
//        // 定义一个list
//        List<Person> personList = new ArrayList<>();
//        for (int i = 0; i < 3; i++) {
//            Person person = new Person();
//            person.setId((long) i);
//            person.setName("张三" + i);
//            person.setSex("男");
//            person.setAge(18);
//            personList.add(person);
//        }
//        Person person2 = new Person();
//        person2.setId(10L);
//        person2.setName("张三10");
//        person2.setSex("女");
//        person2.setAge(18);
//        personList.add(person2);
//
//
//        // 把list通过某个字段变成 key -》 list这种形式
//        Map<String, List<Person>> collect = MapUtils.listToMap(personList, "sex");
//        System.out.println(collect);
//
//        // 定义一个新的list
//        List<Person> newPersonList = new ArrayList<>();
//
//        // 遍历之前的 key -》 list这种形式
//        collect.forEach((key, list) -> {
//            // TODO 通过key去汇总落地和出资
//            list.forEach(item -> {
//                // 计算逻辑
//                item.setCode("test");
//            });
//            newPersonList.addAll(list);
//        });
//        System.out.println(newPersonList);
//
//        // 根据协议时间进行升序排序
//        newPersonList.sort((x, y) -> Double.compare(x.getId(), y.getId()));
//
//        System.out.println(newPersonList);
//
//        // 根据协议时间进行降序排序
//        newPersonList.sort((x, y) -> Double.compare(y.getId(), x.getId()));
//
//        System.out.println(newPersonList);
//    }
}
//
//// 测试实体
//class Person implements Serializable {
//
//    private Long id;
//    private String code;
//    private String name;
//    private String sex;
//    private Integer age;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getSex() {
//        return sex;
//    }
//
//    public void setSex(String sex) {
//        this.sex = sex;
//    }
//
//    public Integer getAge() {
//        return age;
//    }
//
//    public void setAge(Integer age) {
//        this.age = age;
//    }
//}