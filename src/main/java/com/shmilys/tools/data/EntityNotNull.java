package com.shmilys.tools.data;


import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

public class EntityNotNull {
    /**
     * 抛出异常的
     * 实体参数全不为空
     *
     * @param t
     * @param <T>
     */
    public static <T> void entityNotNull(T t) throws NullPointerException {
        Entity<T> entity = new Entity<>(t);
        BiPredicate<Object, String> predicate = throwExceptionPredicate();
        BiFunction<Entity, Field, Boolean> function = nullOutFunction();
        execution(entity, predicate, function);
    }

    /**
     * 抛出异常的
     * 允许下列字段为空
     *
     * @param t         实体
     * @param fieldName 允许为空的字段
     * @param <T>
     */
    public static <T> void entityAllowNull(T t, String... fieldName) throws NullPointerException {
        Entity<T> entity = new Entity<>(t, fieldName);
        BiPredicate<Object, String> predicate = throwExceptionPredicate();
        BiFunction<Entity, Field, Boolean> function = nullOutFunction();
        execution(entity, predicate, function);
    }

    /**
     * 抛出异常的
     * 下列字段不为空
     *
     * @param t         实体
     * @param fieldName 不允许为空的字段
     * @param <T>
     */

    public static <T> void entityNotNull(T t, String... fieldName) throws NullPointerException {
        Entity<T> entity = new Entity(t, fieldName);
        BiPredicate<Object, String> predicate = throwExceptionPredicate();
        BiFunction<Entity, Field, Boolean> function = nullInFunction();
        execution(entity, predicate, function);
    }

    /**
     * 返回boolean 值的
     * 实体参数全不为空
     *
     * @param t
     * @param <T>
     */
    public static <T> boolean returnEntityNotNull(T t) {
        Entity<T> entity = new Entity(t);
        BiPredicate<Object, String> predicate = returnFlagPredicate();
        BiFunction<Entity, Field, Boolean> function = nullOutFunction();
        return execution(entity, predicate, function);
    }

    /**
     * 抛出异常的
     * 允许下列字段为空
     *
     * @param t         实体
     * @param fieldName 允许为空的字段
     * @param <T>
     */
    public static <T> boolean returnEntityAllowNull(T t, String... fieldName) {
        Entity<T> entity = new Entity(t, fieldName);
        BiPredicate<Object, String> predicate = returnFlagPredicate();
        BiFunction<Entity, Field, Boolean> function = nullOutFunction();
        return execution(entity, predicate, function);
    }

    /**
     * 抛出异常的
     * 下列字段不为空
     *
     * @param t         实体
     * @param fieldName 不允许为空的字段
     * @param <T>
     */

    public static <T> boolean returnEntityNotNull(T t, String... fieldName) {
        Entity<T> entity = new Entity(t, fieldName);
        BiPredicate<Object, String> predicate = returnFlagPredicate();
        BiFunction<Entity, Field, Boolean> function = nullInFunction();
        return execution(entity, predicate, function);
    }


    /**
     * 执行主体
     *
     * @param entit
     * @param resultPredicate
     * @param fieldFunction
     * @param <T>
     * @return
     */

    private static <T> boolean execution(Entity<T> entit, BiPredicate<Object, String> resultPredicate, BiFunction<Entity, Field, Boolean> fieldFunction) {
        Field[] fields = entit.getT().getClass().getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            boolean flag = fieldFunction.apply(entit, field);
            if (flag) {
                continue;
            }
            Object val = fieldValueFunction().apply(entit.getT(), field);
            if (!resultPredicate.test(val, field.getName())) {
                return false;
            }
        }
        return true;
    }


    /**
     * 抛出异常的结果处理器
     *
     * @return
     * @throws NullPointerException
     */

    private static BiPredicate<Object, String> throwExceptionPredicate() throws NullPointerException {
        return (result, fieldName) -> {
            if (ObjectUtils.isEmpty(result)) {
                throw new RuntimeException(fieldName + " is null");
            }
            return true;
        };
    }

    /**
     * 返回参数的结果处理器
     *
     * @return
     */

    private static BiPredicate<Object, String> returnFlagPredicate() {
        return (result, fieldName) -> !ObjectUtils.isEmpty(result);
    }

    /**
     * 当字段存在数组中时执行
     *
     * @return
     */


    private static BiFunction<Entity, Field, Boolean> nullInFunction() {
        return (entity, field) -> !entity.contains(field.getName());
    }

    /**
     * 当字段不存在于数组中执行
     *
     * @return
     */

    private static BiFunction<Entity, Field, Boolean> nullOutFunction() {
        return (entity, field) -> entity.contains(field.getName());
    }

    /**
     * 默认取值接口
     *
     * @return
     */
    private static BiFunction<Object, Field, Object> fieldValueFunction() {
        return (obj, field) -> {
            try {
                return field.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                throw new RuntimeException("EntityUtil Get Field Value Error");
            }
        };
    }

    private static class Entity<T> {
        private final T t;
        private final List<String> fieldNames;

        public Entity(T t, String... fieldName) {
            this.t = t;
            this.fieldNames = Arrays.asList(fieldName);
        }

        public T getT() {
            return t;
        }

        public boolean contains(String fieldName) {
            return fieldNames.contains(fieldName);
        }
    }

    public static void main(String[] args) {
        float [][] f1  = {{1.2f,2.3f},{4.5f,5.6f}};
        Object oo = f1;
        f1[1] = (float[]) oo;
        System.out.println(f1[1]);
    }


}

