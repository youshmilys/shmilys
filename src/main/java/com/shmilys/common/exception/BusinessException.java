package com.shmilys.common.exception;

import com.shmilys.tools.format.MessageFormatUtils;
import lombok.Data;

/**
 *
 *   try {
 *
 *         } catch (ErrorException e) {
 *             ErrorHandle.errorExceptionHandle(result, "query", e);
 *         } catch (Exception e) {
 *             ErrorHandle.systemExceptionHandle(result, "delete", e);
 *         }
 *         return result;
 * @author shmilys
 * @date 2020/8/4 21:14
 */
@Data
public class BusinessException extends RuntimeException{
    private final static String ERROR_CODE = "error code: [{0}]";

    protected int code = -1;
    protected String message;
    protected Object[] paras;
    protected Throwable cause = this;

    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public Object[] getParas() {
        return this.paras;
    }

    public  BusinessException(int code) {
        this.code = code;
        this.message = MessageFormatUtils.msgFormat(ERROR_CODE, code);
    }

    public BusinessException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BusinessException(int code, String message, Throwable cause) {
        this.fillInStackTrace();
        this.code = code;
        this.message = message;
        this.cause = cause;
    }

    public BusinessException(int code, Object... paras) {
        this.code = code;
        this.paras = paras;
        this.message = MessageFormatUtils.msgFormat(ERROR_CODE, code);;
    }

    public BusinessException(int code, Throwable cause, Object... paras) {
        this.fillInStackTrace();
        this.code = code;
        this.paras = paras;
        this.message =  MessageFormatUtils.msgFormat(ERROR_CODE, code);;
        this.cause = cause;
    }

    public BusinessException(String message) {
        this.message = message;
    }

    public BusinessException(Throwable cause) {
        this.fillInStackTrace();
        this.message = cause == null ? null : cause.getMessage();
        this.cause = cause;
    }

    public BusinessException(String message, Throwable cause) {
        this.fillInStackTrace();
        this.message = message;
        this.cause = cause;
    }

    @Override
    public Throwable getCause() {
        return this.cause == this ? null : this.cause;
    }
}
