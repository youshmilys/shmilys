package com.shmilys.spring.jpa.util;

/**
 * Like动态查询条件
 *
 */
public class LikeSpecification<T> extends StringSpecification<T> {

    public LikeSpecification(String attrName, String attrValue) {
        super(attrName, attrValue, StringSpecification.LOGICAL_OPERATOR_LIKE);
    }

}