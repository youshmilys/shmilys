package com.shmilys.gson;

import cn.hutool.core.map.MapUtil;
import com.shmilys.tools.reflect.type.TypeBuilder;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.cglib.beans.BeanMap;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GsonUtils extends GsonConfig {
    /**
     * 将对象转成json格式
     * @return String
     */
    public static String objectToGson(Object object) {
        String gsonString = null;
        if (getGson() != null) {
            gsonString = getGson().toJson(object);
        }

        return gsonString;
    }


    /**
     * 将json转成特定的cls的对象
     * @return
     */
    public static <T> T gsonToBean(String gsonString, Class<T> cls) {
        T t = null;
        if (getGson() != null) {
            // 传入json对象和对象类型,将json转成对象
            t = getGson().fromJson(gsonString, cls);
        }
        return t;
    }

    /**
     * json字符串转成list
     * @return
     */
    public static <T> List<T> gsonToList(String gsonString, Class<?> bean) {
        List<T> list = new ArrayList<>();
        if (getGson() != null) {
            //自定义实体变成参数化类型。
            /*Type type = TypeBuilder.newInstance(List.class).beginSubType(bean).endSubType().build();*/

            // 根据泛型返回解析指定的类型,TypeToken<List<T>>{}.getType()获取返回类型
            list.addAll(getGson().fromJson(gsonString, (Type) bean));
        }
        return list;
    }



    /**
     * json 字符串转成Map
     * @param gsonJson 字符串
     * @return
     */
    public static Map gsonToMap(String gsonJson){
        return gsonToCamelCaseToMap(gsonJson,false);
    }

    /**
     * json 字符串转成Map
     * @param gsonJson 字符串
     * @param isCamelCase 是否启用下划线转驼峰命名
     * @return
     */
    public static Map<String,Object> gsonToCamelCaseToMap(String gsonJson, boolean isCamelCase){
        //自定义实体变成参数化类型。
        Type type = TypeBuilder.newInstance(Map.class).endSubType().build();
        Map<String,Object> map = getGson().fromJson(gsonJson, type);
        if (isCamelCase){
            if (!map.isEmpty()){
                return MapUtil.toCamelCaseMap(map);
            }
        }
        return map;
    }


    /**
     * json 字符串转成ListMap
     * @param gsonJson 字符串
     * @param isCamelCase 是否启用下划线转驼峰命名
     * @return
     */
    public static List<Map<String,Object>> gsonToCamelCaseToListMap(String gsonJson, boolean isCamelCase){
        Type type = TypeBuilder.newInstance(List.class).beginSubType(Map.class).addTypeParam(String.class).addTypeParam(Object.class).endSubType().build();
        List<Map<String,Object>> mapList = getGson().fromJson(gsonJson, type);
        List<Map<String,Object>> resultMapList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(mapList)){
            mapList.forEach(resultMap -> {
                if (isCamelCase){
                    resultMapList.add(MapUtil.toCamelCaseMap(resultMap));
                }
            });
            return resultMapList;
        } else {
            return mapList;
        }
    }

    /**
     * list转为json
     *  @return jsons
     */
    public static <T> String listToGson(List<T> ts) {
        return getGson().toJson(ts);
    }

    public static <T> Map<String, Object> beanToMap(T bean) {
        Map<String, Object> map = new HashMap<>();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                map.put(String.valueOf(key), beanMap.get(key));
            }
        }
        return map;
    }

    /**
     * 将map装换为javaBean对象
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, T bean) {
        BeanMap beanMap = BeanMap.create(bean);
        beanMap.putAll(map);
        return bean;
    }

//    public static void main(String[] args) {
//        ArrayList<Object> objects = Lists.newArrayList();
//        objects.stream().map()
//    }


}
