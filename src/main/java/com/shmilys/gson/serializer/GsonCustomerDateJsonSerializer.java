package com.shmilys.gson.serializer;

import com.google.gson.*;
import com.shmilys.tools.reflect.time.StringSmallUtils;

import java.lang.reflect.Type;
import java.util.Date;

public class GsonCustomerDateJsonSerializer implements JsonSerializer<Date>, JsonDeserializer<Date> {
    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(StringSmallUtils.dateToSpecialString(src));
    }

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return StringSmallUtils.specialStringToDate(json.getAsString());
    }
}

