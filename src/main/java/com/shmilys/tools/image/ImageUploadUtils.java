package com.shmilys.tools.image;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.shmilys.common.exception.BusinessException;
import com.shmilys.tools.custom.bean.BeanUtils;
import com.shmilys.tools.format.MessageFormatUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ImageUploadUtils {

    /**
     * 用于多个文件上传，文件的URL用某个符号分割
     *
     * @param imageUrlGroup
     * @return
     */
    public static String jointImageUrl(String[] imageUrlGroup,String split) {
        StringBuilder urlHttps = new StringBuilder();
        if (ArrayUtil.isNotEmpty(imageUrlGroup)) {
            for (String img : imageUrlGroup) {
                if (img.contains("https://")) {
                    urlHttps.append(img).append(split);
                } else {
                    urlHttps.append("https://").append(img).append(split);
                }
            }
            //去除最后符号
            if (imageUrlGroup.length != 0) {
                urlHttps = new StringBuilder(urlHttps.substring(0, urlHttps.length() - 1));
            }
        }
        return urlHttps.toString();
    }

    /**
     * 用于解析字符串拼接得图片url
     *
     * @param images
     * @return
     */
    public static String[] captureImagesUrls(String images, String split) {
        String[] result = null;
        try {
            if (StrUtil.isNotBlank(images)) {
                //以逗号分割，得出的数据存到 result 里面
                result = StringUtils.split(images,split);
            }
        }catch (Exception e){
            log.error(MessageFormatUtils.msgFormat("解析字符串获取图片Url出错 在ImageUtils.parseImage中获取了异常。"));
            return null;
        }
        return result;
    }


    /**
     * 解析存在数据库的字符串多张图片路径url组成的字符串
     *
     * @param images
     * @return
     */
    public static List<ImageUpload> parseImageToImageUpload(String images, String split) {
        List<ImageUpload> imageUploadList = new ArrayList<>();
        if (StrUtil.isNotBlank(images)) {
            //以逗号分割，得出的数据存到 result 里面
            try {
                for (String img : StringUtils.split(images, split)) {
                    ImageUpload imageUpload = new ImageUpload();
                    imageUpload.setUrl(img);
                    imageUpload.setName(img);
                    imageUploadList.add(imageUpload);
                }
            }catch (Exception e){
                log.error(MessageFormatUtils.msgFormat("解析字符串获取图片Url出错 在ImageUtils.parseImage中获取了异常。"));
                return null;
            }
        }
        return imageUploadList;
    }

    /**
     * 解析存在数据库的Json字符串多张图片路径url组成的字符串
     *
     * @param jsonImages
     * @return
     */
    public static List<ImageUpload> parseJsonImageToImageUpload(String jsonImages) {
        List<ImageUpload> imageUploadList = new ArrayList<>();
        if (StrUtil.isNotBlank(jsonImages)) {
            try {
               imageUploadList.addAll(BeanUtils.fromJsonArray(jsonImages,ImageUpload.class));
            }catch (Exception e){
                log.error(MessageFormatUtils.msgFormat("解析Json字符串获取图片Url出错 在ImageUtils.parseJsonImageToImageUpload。"));
                throw new BusinessException(-2, "解析Json字符串获取图片Url出错 在ImageUtils.parseJsonImageToImageUpload,请联系开发人员!");
            }
        }
        return imageUploadList;
    }
}
