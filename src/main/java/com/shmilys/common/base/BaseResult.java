package com.shmilys.common.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 所有功能都可以用于返回Result
 * @author shmilys
 * @date 2020/8/4 21:11
 */
@Data
public class BaseResult<T> implements Serializable {
    protected int code;
    protected String message;
    protected String description;
    protected String warnMessage;
    protected T result;
    public BaseResult(T result) {
        this();
        this.result = result;
    }

    public BaseResult() {
        this(200, "成功", "");
    }

    public BaseResult(int code, String message, String description) {
        this.code = code;
        this.message = message;
        this.description = description;
    }

    public BaseResult(int code) {
        this.code = code;
    }

    public BaseResult(String warnMessage) {
        this();
        this.warnMessage = warnMessage;
    }

    public static BaseResult succResult() {
        BaseResult result = new BaseResult();
        result.code = 200;
        return result;
    }

    public static BaseResult httpOkResult() {
        BaseResult result = new BaseResult();
        result.code = 200;
        result.message = null;
        return result;
    }

    public static BaseResult httpOkResult(String message) {
        BaseResult result = new BaseResult();
        result.code = 200;
        result.message = message;
        return result;
    }

    public static BaseResult error(String msg) {
        BaseResult result = new BaseResult();
        result.code = -1;
        result.message = msg;
        result.description = msg;
        return result;
    }

    public static BaseResult nestSelf(BaseResult original) {
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(original.getCode());
        baseResult.setMessage(original.getMessage());
        baseResult.setDescription(original.getDescription());
        baseResult.setResult(original);
        return baseResult;
    }

    public T getResult() {
        return this.result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public BaseResult result(T result) {
        this.result = result;
        return this;
    }

    public BaseResult errorCode(int code) {
        this.code = code;
        return this;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return this.message;
    }

    public BaseResult setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return "result [code=" + this.code + ", message=" + this.message + ", description=" + this.description + ", result=" + this.result + "]";
    }

    public boolean isSuccess() {
        return 0 == this.getCode() || 200 == this.getCode();
    }

    public BaseResult code(int code) {
        this.code = code;
        return this;
    }

    public BaseResult description(String description) {
        this.description = description;
        return this;
    }

    public BaseResult message(String message) {
        this.message = message;
        return this;
    }

    public String getWarnMessage() {
        return this.warnMessage;
    }

    public void setWarnMessage(String warnMessage) {
        this.warnMessage = warnMessage;
    }
}
