package com.shmilys.common.web;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @author wangcan
 */
public class CookieUtil {
    /**
     * 得到当前request请求的所有cookie
     *
     * @param request cookie数组
     */
    public static Cookie[] getCookies(HttpServletRequest request) {
        return request == null ? null : request.getCookies();
    }

    /**
     * 根据cookie名字取得cookie
     *
     * @return cookie对象
     */
    public static Cookie getCookie(HttpServletRequest request, String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }

        Cookie[] cookies = getCookies(request);
        if (cookies == null) {
            return null;
        }

        for (int i = 0; i < cookies.length; i++) {
            Cookie cookie = cookies[i];
            if (name.equals(cookie.getName())) {
                return cookie;
            }
        }
        return null;
    }

    /**
     * 根据cookie名字取得cookie的值
     *
     * @return cookie的值
     */
    public static String getCookieValue(HttpServletRequest request, String name) {
        Cookie cookie = getCookie(request, name);
        if (cookie != null) {
            return cookie.getValue();
        }
        return null;
    }
}
