package com.shmilys.common.massage;

import com.shmilys.tools.format.MessageFormatUtils;
import org.apache.commons.lang3.StringUtils;


/**
 * 提示文案扩展类
 */
public final class StatusCodeExt {

    private static final String TIPS = "友情提示：";

    private final int statusCode;
    private final String statusReason;

    /**
     *  覆盖提示文案 ：
     *      如果传入的状态有提示文案，则覆盖原有的提示文案。
     *      如果传入的状态没有提示文案，则返回传入的提示状态和提示文案。
     *
     * @param statusCode 状态码
     * @param statusReason 提示文案
     * @param extraCopywriter 提示文案参数值
     * @return
     */
    public static StatusCodeExt fullCoveragePromptCopywriter(int statusCode, final String statusReason,Object... extraCopywriter){
        return promptCopywriter(statusCode, statusReason, false, extraCopywriter);
    }

    /**
     *  原有文案上增加提示文案
     *      如果传入的状态有提示文案，则在原有的提示文案增加提示文案。
     *      如果传入的状态没有提示文案，则返回传入的提示状态和提示文案。
     *
     * @param statusCode 状态码
     * @param statusReason 提示文案
     * @param extraCopywriter 提示文案参数值
     * @return
     */
    public static StatusCodeExt originalIncreasePromptCopywriter(int statusCode, final String statusReason, Object... extraCopywriter){
        return promptCopywriter(statusCode,statusReason,true, extraCopywriter);
    }

    /**
     * 自定义提示文案
     */
    public static StatusCodeExt copywriter(int statusCode,Object... extraCopywriter){
        return StatusCodeExt.builder().setStatusCode(statusCode).setStatusReason(MessageFormatUtils.format(extraCopywriter)).build();
    }

    /**
     *  自定义提示文案 || 改变原有状态的提示文案
     *
     * @param statusCode 状态码
     * @param statusReason 提示文案
     * @param isReserve 是否在原有的文案上额外增加提示文案 :
     *                   true原有上增加  false原有被覆盖
     * @param extraCopywriter 提示文案参数值
     * @return 组合的提示文案
     */
    private static StatusCodeExt promptCopywriter(int statusCode, final String statusReason, boolean isReserve, Object... extraCopywriter) {
        String reason = StatusDescribeEnum.getStatusReason(statusCode);
        if (isReserve){
            if (StringUtils.isNotEmpty(reason)){
                return StatusCodeExt.builder().setStatusCode(statusCode).setStatusReason(reason.concat(TIPS).concat(MessageFormatUtils.msgFormat(statusReason,extraCopywriter))).build();
            }
        }
        return StatusCodeExt.builder().setStatusCode(statusCode).setStatusReason(MessageFormatUtils.msgFormat(statusReason,extraCopywriter)).build();
    }




    /* 建造者模式 */

    private StatusCodeExt(StatusCodeBuilder builder) {
        this.statusCode = builder.statusCode;
        this.statusReason = builder.statusReason;
    }

    public static StatusCodeBuilder builder() {
        return new StatusCodeBuilder();
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public static class StatusCodeBuilder {
        private Integer statusCode;
        private String statusReason;

        private StatusCodeBuilder() {
        }

        public StatusCodeBuilder setStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public StatusCodeBuilder setStatusReason(String statusReason) {
            this.statusReason = statusReason;
            return this;
        }

        public StatusCodeBuilder of(StatusCodeExt statusCodeExt) {
            this.statusCode = statusCodeExt.statusCode;
            this.statusReason = statusCodeExt.statusReason;
            return this;
        }

        public StatusCodeExt build() {
            return new StatusCodeExt(this);
        }
    }
}
