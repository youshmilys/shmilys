package com.shmilys.common.massage;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public enum StatusDescribeEnum {
    OK(200,"成功. "),
    CREATED(201,"已创建,成功请求并创建了新的资源. "),
    BAD_REQUEST(400,"请求错误. "),
    SERVER_ERROR(500,"服务器内部错误，无法完成请求. "),
    UNAUTHORIZED(401,"身份验证失败，请重新验证身份. "),
    PARTIAL_CONTENT(206,"更新了部分内容. ");
    /**
     * 状态码
     */
    private final int statusCode;
    /**
     * 状态描述
     */
    private final Object statusReason;


    public int getStatusCode(){
        return this.statusCode;
    }
    public Object getStatusReason(){
        return this.statusReason;
    }

    StatusDescribeEnum(int statusCode, Object statusReason) {
        this.statusCode = statusCode;
        this.statusReason = statusReason;
    }

    /**
     * 通过状态获取状态描述
     * @param statusCode 状态Code
     * @return 状态描述
     */
    public static String getStatusReason(int statusCode) {
        Object o = parseStatusDescribeEnumToMap().get(statusCode);
        if (Objects.isNull(o)){ return null; }
        return String.valueOf(o);
    }


    /**
     * 获取所有状态以及文案提示内容
     * @return
     */
    public static Map<Integer,Object> parseStatusDescribeEnumToMap(){
        List<StatusDescribeEnum> statusDescribeEnums = Arrays.asList(StatusDescribeEnum.values());
        return statusDescribeEnums.stream().collect(
                Collectors.toMap(StatusDescribeEnum::getStatusCode, StatusDescribeEnum::getStatusReason));
    }
}
