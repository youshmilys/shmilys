package com.shmilys.tools.data;

import com.shmilys.common.base.ObjectData;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * 主要解决操作 ObjectData 空指针问题
 * 不安全用法：data.get("key"); data为null时会报空指针异常
 * 安全用法：ObjectDataUtil.get(data,"key");
 * 其他类型，比如 Map<String, Object> ，可自行进行封装
 * @date 2020/10/26
 **/
public class ObjectDataUtil {

    public static void set(ObjectData data, String key, Object value) {
        set(data, key, value, false);
    }

    /**
     * 安全的set方法
     *
     * @param data
     * @param key
     * @param value
     * @param ifNew 当对象为null时，是否实例化一个对象（需要接受返回值）
     * @return
     */
    public static ObjectData set(ObjectData data, String key, Object value, Boolean ifNew) {
        if (ifNew) {
            if (data != null) {
                data.set(key, value);
            } else {
                data = new ObjectData();
                data.set(key, value);
            }
        } else {
            if (data != null) {
                data.set(key, value);
            }
        }
        return data;
    }

    /**
     * 安全的get方法
     * Object res = ObjectDataUtil.get(data,"key");
     *
     * @param data
     * @param key
     * @return
     */
    public static Object get(ObjectData data, String key) {
        return get(data, key, null);
    }

    /**
     * 可以 链式 获取
     * 不需要判断data是否为null，parent是否为null
     * Object res = ObjectDataUtil.getKeys(data, "parent", "children");
     *
     * @param data
     * @param keys
     * @return
     */
    public static Object getKeys(ObjectData data, String... keys) {
        Optional<ObjectData> optional = Optional.ofNullable(data);
        Optional<ObjectData> obj = optional;
        for (int i = 0; i < keys.length; i++) {
            String key = keys[i];
            if (key != null) {
                if (i < (keys.length - 1)) {
                    obj = obj.map(o -> o.get(key, ObjectData.class));
                } else {
                    return obj.map(o -> o.get(key)).orElse(null);
                }
            }
        }
        return obj.orElse(null);
    }

    /**
     * 可以指定默认返回值
     * Object res = ObjectDataUtil.get(data,"key","我是默认返回对象");
     * Object res = ObjectDataUtil.get(data,"key",new ObjectData());
     *
     * @param data
     * @param key
     * @param defaultObj
     * @return
     */
    public static Object get(ObjectData data, String key, Object defaultObj) {
        // Optional 正常用法
        // return Optional.ofNullable(data).map(o -> o.get(key)).orElse(defaultObj);
        // 终极解决方案
        return resolve(() -> data.get(key)).orElse(defaultObj);
    }

    /**
     * 可以指定默认返回类型
     * String res = ObjectDataUtil.getT(data,"key",String.class);
     *
     * @param data
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getT(ObjectData data, String key, Class<T> clazz) {
        return getT(data, key, clazz, null);
    }

    /**
     * 可以指定默认返回类型和返回值
     * String res = ObjectDataUtil.getT(data,"key",String.class,"我是默认返回字符串");
     *
     * @param data
     * @param key
     * @param clazz
     * @param defaultT
     * @param <T>
     * @return
     */
    public static <T> T getT(ObjectData data, String key, Class<T> clazz, T defaultT) {
        return resolve(() -> data.get(key, clazz)).orElse(defaultT);
    }

    /**
     * 通过 suppiler 函数自定义增强 API
     *
     * @param resolver
     * @param <T>
     * @return
     */
    public static <T> Optional<T> resolve(Supplier<T> resolver) {
        try {
            T result = resolver.get();
            return Optional.ofNullable(result);
        } catch (NullPointerException e) {
            // 可能会抛出空指针异常，直接返回一个空的 Optional 对象
            return Optional.empty();
        }
    }
}
