package com.shmilys.tools.custom.bean;

import cn.hutool.core.bean.BeanUtil;
import com.shmilys.gson.GsonUtils;
import com.shmilys.tools.image.ImageUpload;
import com.shmilys.tools.reflect.type.TypeBuilder;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class    BeanUtils {


    /* ----------------------------------    gson 相关 转换 bean       ------------------------------------*/
    /**
     *  自定义映射类 并获取获取查询结果集
     *  循环变成JavaBean装进集合中
     * @param bean        自定义映射对象
     * @param listResult 查询的结果
     * @param <T>        自定义类型 (限于Java类)
     * @return
     */
    public static <T> List<T> listBeanToListMap(List<Map<String, Object>> listResult, T bean) {
        if (CollectionUtils.isEmpty(listResult)) {
            return null;
        }
        List<T> list = new ArrayList<>();
        listResult.forEach(result -> {
            list.add(GsonUtils.mapToBean(result, bean));
        });
        return list;

    }

    /**
     *  自定义映射类 并获取获取查询结果集
     *  把listResult变成JsonString字符串
     *  再把字符串变成JSONArray循环变成JavaBean
     * @param bean        自定义映射对象
     * @param listResult 查询的结果
     * @param <T>        自定义类型 (限于Java类)
     * @return
     */
    public static <T> List<T> listBeanToJsonStringToListMap(List<Map<String, Object>> listResult, Class<T> bean){
        if (CollectionUtils.isEmpty(listResult)){
            return null;
        }
        return listBeanToJsonString(GsonUtils.objectToGson(listResult), bean);
    }

    /**
     *  自定义映射类 并获取获取查询结果集
     *  把listResult变成JsonString字符串
     * @param bean        自定义映射对象
     * @param jsonString 查询的结果
     * @param <T>        自定义类型 (限于Java类)
     * @return
     */
    public static <T> List<T> listBeanToJsonString(String jsonString, Class<T> bean){
        if (StringUtils.isBlank(jsonString)){
            return null;
        }
        return GsonUtils.gsonToList(jsonString, bean);
    }

    /**
     *  jsonObject字符串转换为自定义对象
     * @param jsonObject
     * @param clazz
     * @param <T>
     * @return object
     */
    public static <T> T fromJsonObject(String jsonObject, Class<T> clazz) {
        return GsonUtils.getGson().fromJson(jsonObject, TypeBuilder.newInstance(clazz).build());
    }

    /**
     * jsonArray字符串转为自定义对象
     * @param jsonArray
     * @param clazz
     * @param <T>
     * @return  list
     */
    public static <T> List<T> fromJsonArray(String jsonArray, Class<T> clazz){
        return GsonUtils.getGson().fromJson(jsonArray, TypeBuilder.newInstance(List.class).beginSubType(clazz).endSubType().build());
    }


    public static void main(String[] args) {
        String jsonObject  = "{\"url\":\"https://appfiles-1302237814.cos.ap-shenzhen-fsi.myqcloud.com/img/1553235753.jpg\",\"name\":\"1553235753.jpg\",\"uid\":1605980862638,\"status\":\"success\"}";
        String jsonArray = "[{\"url\":\"https://appfiles-1302237814.cos.ap-shenzhen-fsi.myqcloud.com/img/1553235752.jpg\",\"name\":\"1553235752.jpg\",\"uid\":1605980862618,\"status\":\"success\"},{\"url\":\"https://appfiles-1302237814.cos.ap-shenzhen-fsi.myqcloud.com/img/1553235753.jpg\",\"name\":\"1553235753.jpg\",\"uid\":1605980862638,\"status\":\"success\"}]";
        List<ImageUpload> imageUpload = fromJsonArray(jsonArray, ImageUpload.class);
        System.out.println("imageUpload = " + imageUpload);
    }

    /*----------------------------  hutool 相关 转换  Bean         -----------------------------------*/

    /**
     * 自定义映射类 并获取获取查询结果集
     *  集合
     * @param clz        自定义映射类
     * @param listResult 查询的结果
     * @param <T>        自定义类型 (限于Java类)
     * @return 结果映射类。
     */
    public static <T> List<T> listCustomBeanToListMap(Class<T> clz, List<Map<String, Object>> listResult) {
        List<T> list = new ArrayList<>();
        listResult.forEach(data -> {
            //Map转换为Bean对象  返回结果值  要转换的bean对象  空也不会报错
            list.add(BeanUtil.mapToBeanIgnoreCase(data, clz, true));
        });
        return list;
    }



    /**
     * 自定义映射类 并获取获取查询结果集
     * 单条
     * @param clz        自定义映射类
     * @param result     查询的结果
     * @param <T>        自定义类型 (限于Java类)
     * @return 结果映射类。
     */
    public static <T> T customBeanToMap(Class<T> clz, Map<String, Object> result) {
        return BeanUtil.mapToBeanIgnoreCase(result, clz, true);
    }



//    public static void main(String[] args) {
//        System.out.println(getValueByPropertyName("data", new M(null, 1)));
//    }


    //    static class Program {
//        public static void main(String[] args) throws JsonProcessingException {
////            Date start = new Date();
////            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonCustomerDateJsonSerializer()).create();
////            String gsonStr = gson.toJson(createUser());
////            Date end = new Date();
////            System.out.println("gsonStr = " + gsonStr);
//            Map<String,Object> map1 = new HashMap<>();
//            map1.put("id",1);
//            map1.put("name","韦海超");
//            map1.put("age",11);
//            map1.put("date",DateTime.now());
//            System.out.println(objectToGson(map1));
//
//        }
//
//        private static EntityTest createUser() {
//            EntityTest test1 = new EntityTest();
//            test1.setId(1);
//            test1.setAge(19);
//            test1.setName("韦海超1");
//            test1.setDate(DateTime.now());
//            return test1;
//        }
//    }
//


}
