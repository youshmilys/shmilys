/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.shmilys.common.base;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页工具
 *
 * @author Shmlilys
 * @date 2018-12-10
 */
public class PageUtils {

    /**
     * List 分页
     */
    public static List toPage(int page, int size, List list) {
        int fromIndex = page * size;
        int toIndex = page * size + size;
        if (fromIndex > list.size()) {
            return new ArrayList();
        } else if (toIndex >= list.size()) {
            return list.subList(fromIndex, list.size());
        } else {
            return list.subList(fromIndex, toIndex);
        }
    }

    /**
     * Page 数据处理，预防redis反序列化报错
     */
    public static Map<String, Object> toPage(Page page) {
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("result", page.getContent());
        map.put("totalElements", page.getTotalElements());
        return map;
    }

    /**
     * 自定义Map集合 并分页
     *
     * @param resultList    展示数据
     * @param totalElements 展示数据的总数
     * @return
     */
    public static Map<String, Object> toPageMap(List<?> resultList, Long totalElements) {
        Map<String, Object> resultMap = new LinkedHashMap<>(2);
        resultMap.put("result", resultList);
        resultMap.put("totalElements", totalElements);
        return resultMap;
    }

    /**
     * 自定义Map集合 不分页
     *
     * @param resultList 展示数据
     * @return
     */
    public static Map<String, Object> toMap(List<?> resultList) {
        Map<String, Object> resultMap = new LinkedHashMap<>(1);
        resultMap.put("result", resultList);
        return resultMap;
    }

    /**
     * 自定义List集合 并分页
     *
     * @param resultList    展示数据
     * @param totalElements 展示数据的总数
     * @return
     */
    public static List toPageList(List resultList, Long totalElements) {
        List list = new ArrayList<>();
        list.add(resultList);
        list.add(totalElements);
        return list;
    }

    /**
     * 自定义List集合 不分页
     *
     * @param resultList 展示数据
     * @return
     */
    public static List toList(List resultList) {
        List list = new ArrayList<>();
        list.add(resultList);
        return list;
    }

    /**
     * 自定义 任意类型 并分页
     *
     * @param object
     * @param totalElements
     * @return
     */
    public static Object toPageMapObject(Object object, Long totalElements) {
        Map<String, Object> resultMap = new LinkedHashMap<>(2);
        resultMap.put("result", object);
        resultMap.put("totalElements", totalElements);
        return resultMap;
    }

    /**
     * 自定义 任意类型 不分页
     *
     * @param object
     * @return
     */
    public static Object toMapObject(Object object) {
        Map<String, Object> resultMap = new LinkedHashMap<>(1);
        resultMap.put("result", object);
        return resultMap;
    }

    /**
     * 自定义分页
     */
    public static Map<String, Object> toPage(Object object, Object totalElements) {
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("result", object);
        map.put("totalElements", totalElements);
        return map;
    }

}
