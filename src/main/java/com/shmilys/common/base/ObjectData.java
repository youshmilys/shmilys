package com.shmilys.common.base;


import com.shmilys.gson.GsonUtils;
import com.shmilys.tools.convert.CustomTypeUtlis;
import com.shmilys.tools.reflect.type.TypeBuilder;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 自定义对象对应的数据
 */
@Data
public class ObjectData implements Serializable {
    private static final long serialVersionUID = -4723291828186119181L;

    private Map<String, Object> map;

    public ObjectData() {
        map = new LinkedHashMap<>();
    }

    public ObjectData(Map<String, Object> map) {
        this.map = map;
    }

    public Object get(String key) {
        return map.get(key);
    }

    public void set(String key, Object value) {
        map.put(key, value);
    }

    public <T> T get(String key, Class<T> clazz) {
        Object value = map.get(key);
        return CustomTypeUtlis.customConvertType(value, clazz);
    }

    public String toJsonString() {
        return GsonUtils.getGson().toJson(map, TypeBuilder.newInstance(Map.class).build());
    }
    public void fromJsonString(String jsonString) {
        this.map =  GsonUtils.getGson().fromJson(jsonString,TypeBuilder.newInstance(Map.class).build());
    }

    @Override
    public String toString() {
        return toJsonString();
    }
}